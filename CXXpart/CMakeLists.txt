cmake_minimum_required(VERSION 3.1)
set(CMAKE_CXX_STANDARD 14)
project(CXXpart)
set(SOURCE_FILES code/BaseException.cpp
        code/BaseException.h
        code/client.cpp
        code/customDate.cpp
        code/customDate.h
        code/login.cpp
        code/login.h
        code/Makefile
        code/material.cpp
        code/material.h
        code/serveur.cpp
        code/Socket.cpp
        code/Socket.h
        code/SocketClient.cpp
        code/SocketClient.h
        code/SocketServer.cpp
        code/SocketServer.h
        code/stump.cpp
        code/stump.h
        code/stumpConfig.h
        code/test.cpp)
add_executable(main ${SOURCE_FILES})