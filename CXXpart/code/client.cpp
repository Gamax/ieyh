#include <iostream>
#include <stdio.h>
#include "stump.h"
#include "material.h"
#include "login.h"
#include "customDate.h"
#include "stumpConfig.h"
#include "SocketClient.h"


using namespace std;

string stumpLogin(string id, string mdp);
string stumpHandMat(string action,string materiel, customDate date);
string stumpListCmd();
string stumpCancelHandMat(string idAction, string nom, string date);
string stumpAskMat(string type,string libelle,string marque,string prix,string accessoires);

SocketClient sc ;


int main ()
{
	char choix = 0;
	string materiel, action , livraison = "L", declassement = "D", reparation = "R";
	material mat ;
	float prix ;

	//Initialisation socket legere
	try
	{	
		sc.Connect(PORT);
	}

	catch (BaseException e)
	{
		cout << e.getMsg() << endl ;
	}

	//Identification
	char * tmp, buffer[MAXSTRING], buf[5], sep[2] ;
	customDate *date ;
	strcpy(sep,SEPARATOR);
	string msg,saisie;
	login user ;

	do
	{
		cout << endl << "Entrer identifiant : " ;
		cin >> saisie;
		user.setNom(saisie) ;	
		try
		{
			cout << endl << "Entrer mot de passe : " ;
			cin >> saisie;
			user.setPswd(saisie) ;
			choix = 1 ;
		}
		catch (BaseException e)
		{
			cout << e.getMsg();
		}
	}while(choix == 0 );

	cout << endl;
	cout << "|--------------------------------------|" << endl ;
	cout << "|   MENU :                             |" << endl ;
	cout << "|   ------                             |" << endl ;
	cout << "|   1. Login                           |" << endl ;
	cout << "|   2. Action sur materiel             |" << endl ;
	cout << "|   3. Lister les commandes            |" << endl ;
	cout << "|   4. Annuler une action sur materiel |" << endl ;
	cout << "|   5. Demande de materiel             |" << endl ;
	cout << "|   6. quitter                         |" << endl ;
	cout << "|--------------------------------------|" << endl ;
	cout << "choisir une option : ";
	do
	{
		cout << "choisir une option : ";
		fflush(stdin);
		cin >> choix ; 

		switch(choix)
		{
			case '1':	msg = stumpLogin(user.getNom(),user.getPswd());
		                	break;

			case '2':	cout << endl << "L : Livraison " << endl ;
					cout << "R : Reparation " << endl ;
					cout << "D : Declassement " << endl ;
					do
					{	
						cout <<  "Id de l'action :" ;
						fflush(stdin) ; 
						cin >> action; 
					}while(action != livraison && action != reparation && action != declassement );

					cout << endl << "Nom du materiel : " ;
					fflush(stdin) ; 
					cin >> materiel ; 
					cout << endl << "Date d'execution : " ;
					fflush(stdin) ; 
					cin >> msg ; 
					 	
					try
					{
						date = new customDate(msg);
						msg = stumpHandMat(action,materiel,(*date).chaine());
					}
					catch (BaseException e)
					{
						cout << e.getMsg()  << endl;
					}
		                	break;

		    	case '3' :    	msg = stumpListCmd();
		                	break;

		    	case '4' : 	cout <<  "Id de l'action a annuler :" ;
					fflush(stdin) ; 
					cin >> action; 

					cout << endl << "Nom du materiel : " ;
					fflush(stdin) ; 
					cin >> materiel ; 
					try
					{
						date = new customDate(msg);
					}
					catch (BaseException e)
					{
						cout << e.getMsg();
					}
					msg = stumpCancelHandMat(action,materiel,(*date).chaine());
		                	break;

		    	case '5' :    	cout <<  "Type de materiel :" ;
					fflush(stdin) ; 
					cin >> msg ; 
					mat.setType(msg); 

					cout << endl << "Nom du materiel : " ;
					fflush(stdin) ; 
					cin >> msg ;
					mat.setLibelle(msg) ; 
					
					cout << endl << "Marque du materiel : " ;
					fflush(stdin) ; 
					cin >> msg ;
					mat.setMarque(msg) ; 


					cout << endl << "Accessoires (facultatif) : " ;
					fflush(stdin) ; 
					cin >> msg ;
					mat.setAccessoires(msg) ; 

					cout << endl << "Prix du materiel : " ;
					fflush(stdin) ; 
					cin >>msg ;

					msg = stumpAskMat(mat.getType(),mat.getLibelle(),mat.getMarque(),msg,mat.getAccessoires());
		                	break;

			case '6'  :	msg = "6" ; 
					break;

			default :	choix = '0' ;
					break;
		}

		if(choix!='6' && choix != '0')
		{
			tmp = new char[msg.size()+1];
		   	strcpy(tmp,msg.c_str());

			system("clear");
			cout << endl;
			cout << "|--------------------------------------|" << endl ;
			cout << "|   MENU :                             |" << endl ;
			cout << "|   ------                             |" << endl ;
			cout << "|   1. Login                           |" << endl ;
			cout << "|   2. Action sur materiel             |" << endl ;
			cout << "|   3. Lister les commandes            |" << endl ;
			cout << "|   4. Annuler une action sur materiel |" << endl ;
			cout << "|   5. Demande de materiel             |" << endl ;
			cout << "|   6. quitter                         |" << endl ;
			cout << "|--------------------------------------|" << endl ;

			if(sc.Send(tmp) == -1 )
				cout << "Erreur d'envoi : " << errno << endl; 
			else
				cout << "Envoi de " << tmp << endl;
			delete (tmp ) ; 
			if(sc.Receive(buffer) == -1 )
				cout << "Erreur a la reception : " << errno << endl ; 
			else
			{
				cout << "Recu : " << buffer << endl ;


				//TODO : traitement reponse
				/*buf = strtok(buffer, &sep[0]) ;
				if (strcmp (buf, "NOK") == 0 )
				{
					buf = strtok(buffer, &sep[0]) ;
					if(strcmp(buf,"1") == 0)
					{
						choix = 0  ;
						do
						{
							cout << endl << "Entrer identifiant : " ;
							cin >> saisie;
							user.setNom(saisie) ;	
							try
							{
								cout << endl << "Entrer mot de passe : " ;
								cin >> saisie;
								user.setPswd(saisie) ;
								choix = 1 ;
							}
							catch (BaseException e)
							{
								cout << e.getMsg();
							}
						}while(choix == 0 );
						stumpLogin(user.getNom(),user.getPswd());
					}
				}*/
			}
		}

	}while(choix != '6');
	sc.Close();


	return 0;
}


string stumpLogin(string id, string mdp)
{
	string tmp("1") ;
	tmp = tmp + SEPARATOR + id + SEPCHAMP + mdp ;
	return tmp ;
}
string stumpHandMat(string action,string materiel, customDate date)
{
	string tmp ("2") ;
	tmp = tmp + SEPARATOR + action + SEPCHAMP + materiel + SEPCHAMP + date.chaine() ;
	return tmp;
}
string stumpListCmd()
{
	string tmp ;
	tmp = "3";
	    return tmp ;
}
string stumpCancelHandMat(string idAction,string nom, string date)
{
	string tmp ("4") ;
	tmp = tmp + SEPARATOR + idAction + SEPCHAMP + nom  + SEPCHAMP + date;
	return tmp ;
}
string stumpAskMat(string type,string libelle,string marque,string prix,string accessoires)
{
	string tmp ("5") ;
	tmp = tmp + SEPARATOR + type + SEPCHAMP + libelle + SEPCHAMP + marque + SEPCHAMP + prix + SEPCHAMP + accessoires ;
	return tmp ;
}

