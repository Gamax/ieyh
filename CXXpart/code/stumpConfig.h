#define LOGIN 1 	//gestionnaire materiel veut se connect
#define HMAT 2		//demande d'une action
#define LISTCMD 3	//demande liste d'actions
#define CHMAT 4		//suppression d'une action commandee
#define ASKMAT 5	//commande de materiel existant ou pas

#define SEPCHAMP "$"
#define SEPARATOR "%"

#define NBTHREAD 5

#define STATE_OK "OK"
#define STATE_NOK "NOK"
#define STATE_DES "DES"

#define PORT 55001
#define MAXSTRING 1000
#define ENDFRAMECHAR #
