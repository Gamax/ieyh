//
// Created by Thomas on 10/3/2018.
//

#include "SocketServer.h"
#include "SocketClient.h"
#include <iostream>
#include <string.h>

using namespace std;

int main(int argc, char** argv){
    char message[100];
    if(argc > 1 && atoi(argv[1]) ==  1){
        cout << "Mode server" <<endl;

        cout << "Creation socket server" <<endl;
        SocketServer* listenSocket = new SocketServer();
        SocketServer* connectionSocket;
        cout << "Bind Server" <<endl;
        listenSocket->Bind(8000);
        cout << "Listen server" <<endl;
        listenSocket->Listen();
        cout << "Accept server" <<endl;
        connectionSocket = listenSocket->Accept();
        cout << "Receive server" <<endl;
        connectionSocket->Receive(message);
        cout << "Received message from client " << message <<endl;
        strcpy(message,"Hello I'm the server");
        connectionSocket->Send(message);
        connectionSocket->Close();
        listenSocket->Close();

    }else{
        cout << "Mode client" <<endl;
        cout << "Creation socket server" <<endl;
        SocketClient* clientSocket = new SocketClient();
        cout << "Connect client" <<endl;
        clientSocket->Connect(8000);
        cout << "Send client" <<endl;
        clientSocket->Send((char*)"hello i'm the client");
        clientSocket->Receive(message);
        cout << "Received message from server " << message <<endl;
        clientSocket->Close();

    }
}
