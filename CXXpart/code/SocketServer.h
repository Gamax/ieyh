//
// Created by thoma on 9/29/2018.
//

#ifndef PROJECT_SOCKETSERVER_H
#define PROJECT_SOCKETSERVER_H

#include "Socket.h"
#include "BaseException.h"


class SocketServer : public Socket {

private:

public:

    SocketServer(); //Crée socket serveur
    void Bind(unsigned short Port) throw (BaseException);
    void Listen() throw (BaseException);
    SocketServer* Accept() throw (BaseException);


};


#endif //PROJECT_SOCKETSERVER_H
