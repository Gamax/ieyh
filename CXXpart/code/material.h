#ifndef MATERIAL_H
#define MATERIAL_H
#include <iostream>
#include <string.h>
#include "BaseException.h"
using namespace std;

class material {

    private :
    string type ;
    string libelle ;
    string marque;
    float prix ;
    string accessoires;
    string etat ;

    public :
    material(void);
    material(const string Type, const string lib, const string marque, const float Prix, const string acc, const string Etat);
    material(material& original);
    ~material();

    string getType() const;
    string getLibelle() const;
    string getMarque() const ;
    float getPrix() const;
    string getAccessoires() const;
    string getEtat() const ;

    void setType(const string Type);
    void setLibelle(const string Libelle);
    void setMarque(const string Marque);
    void setPrix(const float Prix) throw (BaseException);
    void setAccessoires(const string Acc);
    void setEtat(const string Etat);

    bool operator==(material& mat) const;

    friend ostream& operator<<(ostream& s,const material& mat);
    ostream& Flux(ostream& s) const;
};

#endif // MATERIAL_H
