#ifndef BASEEXCEPTION_H
#define BASEEXCEPTION_H
#include <iostream>
#include <exception>

using namespace std;

class BaseException : public exception
{
	friend ostream& operator<<(ostream& s,const BaseException& erreur);

	protected:
	string msg;

	public:
	//constructeurs
	BaseException(void);
	BaseException(string message);
	//destructeurs
	virtual ~BaseException(void) throw();
	//setters
	virtual void setMsg(string message);
	//getters
	string getMsg() const;
    const char *  what() const throw(); //todo utiliser what à la place de getMsg(), what est standard


};

#endif
