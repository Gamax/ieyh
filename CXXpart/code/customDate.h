#ifndef CUSTOMDATE_H
#define CUSTOMDATE_H
#include <iostream>
#include "BaseException.h"

class customDate
{
    private :
    int jour;
    int mois;
    int annee;

    public :
    customDate();
    customDate(const int j,const int m,const int a);
    customDate(string s) ;
    customDate(const customDate& Date);
    ~customDate();

    int getJour() const ;
    int getMois() const ;
    int getAnnee() const ;

    void setJour(int j) throw (BaseException);
    void setMois(int m) throw (BaseException);
    void setAnnee(int a);
    void setDate(int j, int m, int a);

    bool operator==(customDate& date) const ;
    bool operator<=(customDate& date) const ;
    bool operator>=(customDate& date) const ;
    bool operator<(customDate& date) const ;
    bool operator>(customDate& date) const ;

    customDate add(int nb, char format);
    int nbJourDansMois(int mois,int annee);

    string chaine() const ;

    friend ostream& operator<<(ostream& s,const customDate& mat);
    ostream& Flux(ostream& s) const;
};

#endif // CUSTOMDATE_H
