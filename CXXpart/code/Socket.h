//
// Created by thoma on 9/29/2018.
//

#ifndef PROJECT_SOCKET_H
#define PROJECT_SOCKET_H

class Socket {

protected:
    int SocketDescriptor;

public:
    Socket(); //création socket
    int Send(char * message) ;
    int Receive(char * message) ;
    void Close();


};


#endif //PROJECT_SOCKET_H
