//
// Created by thoma on 9/29/2018.
//

#include "Socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <cstring>

#define MAXSTRING 1000


Socket::Socket() {
    SocketDescriptor = socket(AF_INET,SOCK_STREAM,0);
    //todo gerer erreur
}

int Socket::Send(char *message) {
    return send(SocketDescriptor,message,MAXSTRING,0);
}

int Socket::Receive(char *message) {
    return recv(SocketDescriptor,message,MAXSTRING,0);
}

void Socket::Close() {
    close(SocketDescriptor);
}
