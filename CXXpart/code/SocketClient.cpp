//
// Created by thoma on 9/29/2018.
//

#include "SocketClient.h"
#include "Socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include "BaseException.h"

SocketClient::SocketClient() : Socket() {

}

void SocketClient::Connect(unsigned short Port) throw (BaseException)
{


    struct hostent * infosHost;
    struct sockaddr_in socketAdress;

    char hostname[1024];
    gethostname(hostname, 1023);
    hostname[1023] = '\0';
    if((infosHost = gethostbyname(hostname))==0)
    {
        throw BaseException("Error Bind : gethostbyname failed " + to_string(errno));
    }

    //preparation sock_addr_in
    memset(&socketAdress,0, sizeof(struct sockaddr_in));
    socketAdress.sin_family = AF_INET;
    socketAdress.sin_port = htons(Port); //htons converti le nombre en format réseau
    memcpy(&socketAdress.sin_addr,infosHost->h_addr,infosHost->h_length);

    if(connect(SocketDescriptor,(struct sockaddr *)&socketAdress, sizeof(struct sockaddr_in))==-1){
        throw BaseException("Error Connect : connect() failed " + to_string(errno));
    }
}
