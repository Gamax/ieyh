#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "login.h"
#include "stumpConfig.h"
using namespace std;

login::login(void)
{
    setNom("user");
    setPswd("P@ssw0rd");
}

login::login(const string log,const string pswd)
{
    setNom(log);
    setPswd(pswd);
}

login::login(login& original)
{
    setNom(original.getNom());
    setPswd(original.getPswd());
}

login::~login()
{
}

string login::getNom() const {   return nom; }
string login::getPswd() const {   return mdp; }

void login::setNom(const string log) throw (BaseException)
{
    if(log.size() < 1) throw BaseException("Le nom doit faire au moins 1 caractere ");
    nom = log;
}

void login::setPswd(const string pswd) throw (BaseException)
{
    if(pswd.size() < 5) throw BaseException("Le mot de passe doit faire au moins 5 caractere ");
    mdp = pswd;
}

bool login::operator==(login& log) const
{
    if(getNom()==log.getNom() && getPswd()==log.getPswd())
        return true;
    return false;
}


ostream& operator<<(ostream& s,const login& log)
{
    s << log.getNom() << SEPCHAMP << log.getPswd();
	return s;
}

ostream& login::Flux(ostream& s) const
{
	s<<*(this)<<endl;
	return s;
}

