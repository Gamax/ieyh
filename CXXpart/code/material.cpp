#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "material.h"
#include "stumpConfig.h"
using namespace std;

material::material(void)
{
    setType("/");
    setLibelle("/");
    setMarque("/");
    setPrix(0);
    setAccessoires("/");
    setEtat(STATE_OK);
}

material::material(const string Type, const string lib, const string Marque, const float Prix, const string acc, string Etat)
{
    setType(Type);
    setLibelle(lib);
    setMarque(Marque);
    setPrix(Prix);
    setAccessoires(acc);
    setEtat(Etat);
}

material::material(material& original)
{
    setType(original.getType());
    setLibelle(original.getLibelle());
    setMarque(original.getMarque());
    setPrix(original.getPrix());
    setAccessoires(original.getAccessoires());
    setEtat(original.getEtat());
}

material::~material()
{
}


string material::getType() const {    return type; }
string material::getLibelle() const { return libelle; }
string material::getMarque() const { return marque ; }
float material::getPrix() const {   return prix; }
string material::getAccessoires() const { return accessoires; }
string material::getEtat() const { return etat ; }

void material::setType(const string Type) { type = Type ; }
void material::setLibelle(const string Libelle) { libelle = Libelle ; }
void material::setMarque(const string Marque) { marque = Marque ; }
void material::setPrix(const float Prix) throw (BaseException)
{
    if(Prix < 0 ) throw BaseException("Le prix ne peut pas etre negatif");
    prix = Prix ;
}
void material::setAccessoires(const string Acc) { accessoires = Acc ; }
void material::setEtat(const string Etat) { etat = Etat ; }

bool material::operator==(material& mat) const
{
    if(getType() == mat.getType() && getLibelle() == mat.getLibelle() && getMarque() == mat.getMarque() && getPrix() == mat.getPrix() && getAccessoires() == mat.getAccessoires() && getEtat() == mat.getEtat( ))
        return true;
    return false;
}

ostream& operator<<(ostream& s,const material& mat)
{

        s << mat.getType() << SEPCHAMP << mat.getLibelle() << SEPCHAMP << mat.getMarque() << SEPCHAMP << mat.getPrix()<< SEPCHAMP << mat.getType() << SEPCHAMP << mat.getEtat() ;
	return s;
}

ostream& material::Flux(ostream& s) const
{
    s<<*(this)<<endl;
	return s;
}


