#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "customDate.h"

customDate::customDate()
{
    setAnnee(2018);
    setMois(10);
    setJour(5);
}

customDate::customDate(const int j,const int m,const int a)
{
    setAnnee(a);
    setMois(m);
    setJour(j);
}

customDate::customDate(const customDate& Date)
{
    setAnnee(Date.getAnnee());
    setMois(Date.getMois());
    setJour(Date.getJour());
}

customDate::customDate(string s) 
{
    int j,m,a ;

    sscanf(s.c_str(),"%d / %d / %d",&j,&m,&a);
    setAnnee(a);
    setMois(m);
    setJour(j);
}

customDate::~customDate(){ }

int customDate::getJour() const {return jour ;}
int customDate::getMois() const {return mois ;}
int customDate::getAnnee() const {return annee;}

void customDate::setJour(int j)  throw (BaseException)
{
    if (j < 1) throw BaseException("Le nombre de jours ne peut pas etre inferieur a 1");
    if (j > nbJourDansMois(getMois(),getAnnee())) throw BaseException("Le nombre de jours excede le nombre de jour du mois");
    jour = j ;
}

void customDate::setMois(int m)  throw (BaseException)
{
    if(m < 1) throw BaseException("Un mois ne peut pas etre inferieur a 1");
    if(m > 12) throw BaseException("Un mois ne peut pas etre superieur a 12");
    mois = m ;
}

void customDate::setAnnee(int a){ annee = a ; }

void customDate::setDate(int j, int m, int a)
{
    setAnnee(a);
    setMois(m);
    setJour(j);
}

bool customDate::operator==(customDate& date) const
{
    if(getAnnee() == date.getAnnee() && getMois() == date.getMois() && getJour() == date.getJour())
        return true ;
    return false ;
}

bool customDate::operator<=(customDate& date) const
{
    if(getAnnee() > date.getAnnee() )
        return false ;
    if(getMois() > date.getMois())
        return false ;
    if(getJour() > date.getJour())
        return false;
    return true ;
}

bool customDate::operator>=(customDate& date) const
{
    if(getAnnee() < date.getAnnee() )
        return false ;
    if(getMois() < date.getMois())
        return false ;
    if(getJour() < date.getJour())
        return false;
    return true ;
}

bool customDate::operator<(customDate& date) const
{
    if(getAnnee() < date.getAnnee() )
        return true ;
    if(getMois() < date.getMois())
        return true ;
    if(getJour() < date.getJour())
        return true;
    return false ;
}

bool customDate::operator>(customDate& date) const
{
    if(getAnnee() > date.getAnnee() )
        return true ;
    if(getMois() > date.getMois())
        return true ;
    if(getJour() > date.getJour())
        return true;
    return false ;
}

customDate customDate::add(int nb, char format)
{
    int i = 0, j = 0 ;
    switch(format)
    {
        case 'j' :
        case 'J' :  if(getJour() + nb > nbJourDansMois(getMois(),getAnnee()))
                    {
                        if(getMois() == 12 )
                            j = 1 ;
                        nb = nbJourDansMois(getMois(),getAnnee()) - getJour() ;
                        i = 1 ;
                        while(nb > nbJourDansMois(getMois()+i,getAnnee()+j))
                        {
                            nb =  nbJourDansMois(getMois()+i,getAnnee()+j) ;
                            if (getMois()+i == 12)
                                j ++ ;
                            i ++;
                        }
                    }
                    else
                        nb = getJour() + nb ;
                    setAnnee( getAnnee() + j );
                    setMois( getMois() + i );
                    setJour( nb );
                    break;
        case 'm' :
        case 'M' :  if(getMois() + nb > 12 )
                    {
                        nb = nb - getMois();
                        i = 1 ;
                        while( nb > 12 )
                        {
                            nb = nb - 12 ;
                            i++;
                        }
                    }
                    else
                        nb = getMois() + nb ;
                    setAnnee( getAnnee() + i );
                    setMois( nb );
                    break;
        case 'a' :
        case 'A' :  setAnnee( getAnnee() + nb );
                    break;
    }
    return *this;
}

int customDate::nbJourDansMois(int mois,int annee)
{
    if( mois > 7 )
    {
        if( mois % 2 == 0 )
            return 31 ;
        else
            return 30 ;
    }
    else
    {
        if( mois % 2 != 0 )
            return 31 ;
        else
        {
            if(mois == 2 )
            {
                if(annee % 4 == 0)
                {
                    if(annee %100 == 0 )
                    {
                        if (annee % 400 == 0 )
                            return 29;
                        else
                            return 28 ;
                    }
                    else
                        return 29;
                }
                return 28 ;
            }
            else
                return 30 ;
        }
    }
}

string customDate::chaine() const 
{
    char buf[11];
    sprintf(buf,"%d/%d/%d",getJour(),getMois(),getAnnee());
    string tmp (buf);
    return tmp ;
}

ostream& operator<<(ostream& s,const customDate& mat)
{
    if( mat.getJour() < 10 )
        s << "0" ;
    s << mat.getJour() << "/" ;

    if( mat.getMois() < 10 )
        s << "0" ;
    s << mat.getMois() << "/" ;

    if(mat.getAnnee() < 1000 )
    {
        if(mat.getAnnee() < 100 )
        {
            if(mat.getAnnee() < 10 )
                s << "000" ;
            else
                s << "00" ;
        }
        else
            s << "0" ;
    }
    s << mat.getAnnee();
    return s ;
}

ostream& customDate::Flux(ostream& s) const
{
    s<<*(this)<<endl;
	return s;
}

