//
// Created by thoma on 9/29/2018.
//

#ifndef PROJECT_SOCKETCLIENT_H
#define PROJECT_SOCKETCLIENT_H

#include "Socket.h"
#include "BaseException.h"


class SocketClient: public Socket{

public:
    SocketClient();

    void Connect(unsigned short Port) throw (BaseException);

};


#endif //PROJECT_SOCKETCLIENT_H
