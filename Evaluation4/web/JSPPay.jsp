<%-- 
    Document   : JSPPay
    Created on : Nov 28, 2018, 5:09:23 PM
    Author     : Thomas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% String userid = (String) session.getAttribute("userid"); %>
    Connecté en tant que <%=userid %>
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="logOut">
            <P><input type="submit" value="Log Out"></P>
        </form>
        <br>
        <h1>Paiement</h1>
        
        <% if(request.getAttribute("caddie") != null) { %>
        <h2>Caddie</h2>
        
        <%=request.getAttribute("caddie")%>
        
        <% } %>
        
        <h2>Prix Total : <%=request.getAttribute("price") %></h2>
        
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="paiement">
            <P>Numéro de carte de crédit : <input type="text" name="numCarte" size=20></P>
            <P><input type="submit" value="Payer"></P>
        </form>
        
        <% if(request.getAttribute("message") != null) { %>
        <P><%=request.getAttribute("message")%><P>
        <% } %>
        
    </body>
</html>
