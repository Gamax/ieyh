<%-- 
    Document   : JSPCaddie
    Created on : Nov 28, 2018, 5:09:11 PM
    Author     : Thomas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSPCaddie</title>
    </head>
    <body>
        
        <% String userid = (String) request.getAttribute("userid"); %>
        Connecté en tant que <%=userid %>
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="logOut">
            <P><input type="submit" value="Log Out"></P>
        </form>
        <br>
        
        <% if(request.getAttribute("catalog") != null) { %>
        <h2>Catalogue</h2>
        
        <%=request.getAttribute("catalog")%>
        
        <% } %>
        <% if(request.getAttribute("caddie") != null) { %>
        <h2>Caddie</h2>
        
        <%=request.getAttribute("caddie")%>
        
        <% } %>
        
        <%-- Afficher caddie --%>
        
        
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="addPanier">
            <P>Id de chambre : <input type="text" name="numChambre" size=20></P>
            <P>Date début : <input type="text" name="dateDebut" size=20></P>
            <P>Date fin : <input type="text" name="dateFin" size=20></P>         
            <P><input type="submit" value="Ajouter au panier"></P>
        </form>
        
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="endPanier">        
            <P><input type="submit" value="Cloturer panier"></P>
        </form>
        
        <% if(request.getAttribute("message") != null) { %>
        <P><%=request.getAttribute("message")%><P>
        <% } %>
        
    </body>
</html>
