/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package ServletsPackage;

import DBFacilities.JDBCRequests;
import DBFacilities.RequestClient;
import Formatage.LectureResultSet;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sun.security.pkcs11.Secmod;

/**
 *
 * @author Thomas
 */
public class ControlServlet extends HttpServlet {
    
    private JDBCRequests jdbcRequests;
    
    private RequestClient SRVLink ;
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        try {
            jdbcRequests = new JDBCRequests();
            SRVLink = new RequestClient("0.0.0.0",50100);
            
        } catch (NullPointerException | SQLException | ClassNotFoundException | IOException ex) {
            Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession(true);
        String userid = (String) session.getAttribute("userid");
        
        if(userid==null){
            //on redirige le user sur la page d'accueil si il est pas log
            out.println("<html><META http-equiv=\"refresh\" content=\"0;URL=index.html\"></html>");
            return;
        }
        
        String requestType = request.getParameter("requestType");
        
        
        if("createPanier".equals(requestType))
        {
            try {
                String newUser = request.getParameter("newUser");
                if(newUser != null){
                    String login = request.getParameter("login");
                    String password = request.getParameter("password");
                    String nom = request.getParameter("nom");
                    String prenom = request.getParameter("prenom");
                    String address = request.getParameter("address");
                    String nationalite = request.getParameter("nationalite");
                    String naissance = request.getParameter("naissance");
                    String mail = request.getParameter("mail");
                    String idClient;
                    try {
                        idClient = jdbcRequests.addClientID(nom,prenom,address, nationalite, naissance, mail, null);
                        jdbcRequests.addUser(login,password,idClient);
                    } catch (SQLException ex) {
                        Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                //créer panier
                
                request.setAttribute("catalog", LectureResultSet.HTMLRequestTable(jdbcRequests.GetAllChambres()));
                request.setAttribute("userid", session.getAttribute("userid"));
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/JSPCaddie.jsp");
                rd.forward(request, response);
                return;
            } catch (SQLException ex) {
                Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if("addPanier".equals(requestType))
        {
            try {
                String numChambre = request.getParameter("numChambre");
                String dateDebut = request.getParameter("dateDebut");
                String dateFin = request.getParameter("dateFin");
                //ajouter chambre au panier
                if(jdbcRequests.addReservation(jdbcRequests.getUserClientID(userid), numChambre, dateDebut, dateFin) == null)
                {
                    //impossible de réserver la chambre
                    request.setAttribute("message", "Impossible de réserver la chambre");
                }else{
                    request.setAttribute("message", "Chambre " + numChambre + " ajoutée au panier");
                }
                
                request.setAttribute("catalog", LectureResultSet.HTMLRequestTable(jdbcRequests.GetAllChambres()));
                request.setAttribute("caddie", LectureResultSet.HTMLRequestTable(jdbcRequests.GetPanier(userid)));
                request.setAttribute("userid", session.getAttribute("userid"));
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/JSPCaddie.jsp");
                rd.forward(request, response);
                return;
            } catch (SQLException|ParseException ex) {
                Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if("endPanier".equals(requestType))
        {
            try {
                //cloturer panier
                request.setAttribute("caddie", LectureResultSet.HTMLRequestTable(jdbcRequests.GetPanier(userid)));
                request.setAttribute("userid", session.getAttribute("userid"));
                request.setAttribute("price", jdbcRequests.GetPanierPrix(userid));
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/JSPPay.jsp");
                rd.forward(request, response);
                return;
            } catch (SQLException ex) {
                Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if("logOut".equals(requestType))
        {
            try {
                //todo delog user, pensez à cloture ses trucs
                jdbcRequests.viderPanier(userid);
            } catch (SQLException ex) {
                Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            session.invalidate();
            out.println("<html><META http-equiv=\"refresh\" content=\"0;URL=index.html\"></html>");
            return;
        }
        
        if("paiement".equals(requestType))
        {
            //stocker infos paiement
            String numCarte = request.getParameter("numCarte");
            String message = "Votre paiement a été enregistré";
            
            try {
                message = SRVLink.PayPanier(userid, numCarte);
                
            } catch(IOException ex)
            {
                Logger.getLogger(ControlServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            request.setAttribute("message", message);
            request.setAttribute("userid", session.getAttribute("userid"));
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/JSPInit.jsp");
            rd.forward(request, response);
            return;
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
