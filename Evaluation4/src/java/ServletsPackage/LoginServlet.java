/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletsPackage;

import DBFacilities.JDBCRequests;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Thomas
 */
public class LoginServlet extends HttpServlet {
    
    private JDBCRequests jdbcRequests;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        try {
            jdbcRequests = new JDBCRequests();
        } catch (NullPointerException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        boolean checkPwd = false;
        
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String newClient = request.getParameter("newClient");
        HttpSession session = request.getSession(true);
        
        if(newClient != null){
            request.setAttribute("newUser", "on");
            request.setAttribute("login", login);
            request.setAttribute("password", password);
            session.setAttribute("userid", login);
            request.setAttribute("userid", session.getAttribute("userid"));
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/JSPInit.jsp");
            rd.forward(request, response);
            return;
        }else{
                       
            try {
                checkPwd = jdbcRequests.checkPasswordUser(login, password);
            } catch (SQLException ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex); //todo wtf is that ?
            }
        
        }
        
        
        String userid = (String) session.getAttribute("userid");
        if(userid == null && checkPwd == true){
            userid = login; //todo get user id from db
            session.setAttribute("userid", userid);
        }
        if(userid == null){
            //si mauvais mdp on el redirige sur la page d'accueil
            out.println("<html><META http-equiv=\"refresh\" content=\"0;URL=index.html\"></html>");
            return;
        }
        
        request.setAttribute("userid", session.getAttribute("userid"));
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/JSPInit.jsp");
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
