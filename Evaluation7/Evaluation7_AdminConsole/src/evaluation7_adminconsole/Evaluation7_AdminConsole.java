/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluation7_adminconsole;

import GUI.AdminManager;

/**
 *
 * @author Steph
 */
public class Evaluation7_AdminConsole {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            AdminManager manager = new AdminManager();
            manager.init();
        } catch (Exception e) {
            System.out.println("MAIN : Exception catched : ");
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            System.out.println("Closing...");
            System.exit(0);
        }
        
    }
    
}
