/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Formatage.LectureResultSet;
import HSA.HSAClient;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author Steph
 */
public class AdminManager {
    
    private GUIAdmin gui ;
    private HSAClient hsa ;
    
    public AdminManager()
    {
        hsa = new HSAClient();
    }
    public void init()
    {
        gui = new GUIAdmin();
        gui.setManager(this);
        gui.setVisible(true);
    }
    
    public void connect(String ip, int port, String username, String password)
    {
        try {
            
            hsa.connect(ip, port);
            if(hsa.loginA(username, password))
            {
                gui.setConnected();
                JOptionPane.showMessageDialog(gui, "Connected");
            }
            else
            {
                hsa.disconnect();
                JOptionPane.showMessageDialog(gui, "Cannot connect : invalid credentials");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gui, "Cannot connect : " + ex.getCause() + " : " + ex.getMessage());
            gui.setDisconnected();
            try {
                hsa.disconnect();
            } catch (IOException ex1) {
                Logger.getLogger(AdminManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void disconnect()
    {
        try {
            hsa.disconnect();
            gui.setDisconnected();
            JOptionPane.showMessageDialog(gui, "Disconnected");
        } catch (IOException ex) {
            try {
                JOptionPane.showMessageDialog(gui, "Cannot disconnect : " + ex.getCause() + " : " + ex.getMessage());
                gui.setDisconnected();
                hsa.disconnect();
            } catch (IOException ex1) {
                Logger.getLogger(AdminManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        catch(NullPointerException ex)
        {
            System.out.println("Closing...");
        }
    }
    
    
    
    public void getUtilisateurs()
    {
        try {
            String clients = hsa.LClients();
            
            System.out.println(clients);
            
            gui.setList(LectureResultSet.formatageStringToList(clients));
            
        } catch (IOException ex) {
            try {
                JOptionPane.showMessageDialog(gui, "Cannot get users : " + ex.getCause() + " : " + ex.getMessage());
                
                gui.setDisconnected();
                hsa.disconnect();
            } catch (IOException ex1) {
                Logger.getLogger(AdminManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void pause()
    {
        try {
            hsa.pause();
        } catch (IOException ex) {
            try {
                JOptionPane.showMessageDialog(gui, "Cannot pause : " + ex.getCause() + " : " + ex.getMessage());
                
                gui.setDisconnected();
                hsa.disconnect();
            } catch (IOException ex1) {
                Logger.getLogger(AdminManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    public void stop(int nbSecondes)
    {
        try {
            hsa.stop(nbSecondes);
        } catch (IOException ex) {
            try {
                JOptionPane.showMessageDialog(gui, "Cannot stop : " + ex.getCause() + " : " + ex.getMessage());
                
                gui.setDisconnected();
                hsa.disconnect();
            } catch (IOException ex1) {
                Logger.getLogger(AdminManager.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
}
