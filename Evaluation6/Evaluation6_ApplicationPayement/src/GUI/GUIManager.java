/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Formatage.LectureResultSet;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import GUIFacilities.GUILogin ;
import HSA.EmergencyAdminPort;
import HSA.TraitementEmergencyAdmin;
import SPAYMAPLib.ClientSPAYMAP;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JFrame;
/**
 *
 * @author Steph
 */
public final class GUIManager implements EmergencyAdminPort{
    private GUIPayementClient gui ;
    private ClientSPAYMAP NETLink ; 
    boolean flagPause = false ;
    String portAdmin ;
    
    public GUIManager()
    {
        NETLink = new ClientSPAYMAP("../../Evaluation6/keystore/AppPay.p12");
        gui=new GUIPayementClient();
        
    }
    
    public void init() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        
        GUILogin login = new GUILogin(new JFrame(),true);
        do 
        {                
            login.setVisible(true);
            if(login.getEndState()!=true)
                System.exit(0);
        } while (NETLink.authenticate(login.getUser(), login.getPassword())==false);
        DefaultTableModel dtm = LectureResultSet.formatageStringToTable(NETLink.handshake());
        
        TraitementEmergencyAdmin threadAdmin = new TraitementEmergencyAdmin(this);
        ExecutorService threadPool = Executors.newFixedThreadPool(1);
        threadPool.execute(threadAdmin);
        gui.setManager(this);
        gui.setTable(dtm);
        gui.setVisible(true);
        
    }
    
    
    public void setReservations()
    {
        if(flagPause)
        {
            JOptionPane.showMessageDialog(null,"Serveur has been paused ! No action available !");
            return ;
        }
        
        DefaultTableModel dtm = new DefaultTableModel();
        try {
            dtm = LectureResultSet.formatageStringToTable(NETLink.getAllReservations());
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(GUIManager.class.getName()).log(Level.SEVERE, null, ex);
            dtm.addColumn("Identifiant");
            dtm.addColumn("Client");
            dtm.addColumn("Chambre");
            dtm.addColumn("Date d'arrivée");
            dtm.addColumn("Date de départ");
            dtm.addColumn("Total à payer");
            dtm.addColumn("Montant déjà payé");
        }
        gui.setTable(dtm);
    }
    
    public void setReservations(String prenom, String nom)
    {
        if(flagPause)
        {
            JOptionPane.showMessageDialog(null,"Serveur has been paused ! No action available !");
            return ;
        }
        
        DefaultTableModel dtm = new DefaultTableModel();
        try {
            dtm = LectureResultSet.formatageStringToTable(NETLink.getReservationsPar(prenom, nom));
        } catch (IOException  | ClassNotFoundException ex) {
            Logger.getLogger(GUIManager.class.getName()).log(Level.SEVERE, null, ex);
            dtm.addColumn("Identifiant");
            dtm.addColumn("Client");
            dtm.addColumn("Chambre");
            dtm.addColumn("Date d'arrivée");
            dtm.addColumn("Date de départ");
            dtm.addColumn("Total à payer");
            dtm.addColumn("Montant déjà payé");
        }
        gui.setTable(dtm);
    }
    
    public void payReservation(String reservation, String carte, String Montant)
    {
        if(flagPause)
        {
            JOptionPane.showMessageDialog(null,"Serveur has been paused ! No action available !");
            return ;
        }
            
        
        try{
            double montant = Double.parseDouble(Montant);
            if (montant < 0)
            {
                JOptionPane.showMessageDialog(null,"Error : specified amount cannot be negative !");
                return ;
            }
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null,"Error : montant must be a number !");
            return ;
        }
        
        try {
            String str = NETLink.payer(reservation, carte, Montant); 
            JOptionPane.showMessageDialog(null, str);
            setReservations();
        } catch (Exception ex) {
             JOptionPane.showMessageDialog(null,"Erreur lors du payement");
        }
    }

    
    @Override
    public void setPause() {
        JOptionPane.showMessageDialog(null, "Server has been paused !");
        flagPause = true ;
    }

    @Override
    public void setStop() {
        JOptionPane.showMessageDialog(null, "Server has been disconnected !");
        System.exit(0);
    }

    @Override
    public void setPortAdmin(String ip) {
        portAdmin = ip ;
        //TODO : Fix bug : java.io.StreamCorruptedException: invalid type code: F4 
        //NETLink.giveAdminPort(portAdmin); 
    }

    @Override
    public String getPortClient() {
        return NETLink.getClientPort();
    }
    
    @Override
    public void quit() {
        System.exit(0);
    }

    
}
