/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author thoma
 */
public class testXMLParsing {
    public static void main(String[] args) {
        
        
        try {
            ConfigData configData = new ConfigData();
            System.out.println(configData);
        } catch (SAXException ex) {
            Logger.getLogger(testXMLParsing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(testXMLParsing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testXMLParsing.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
