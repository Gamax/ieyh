
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import DBFacilities.JDBCRequests;
import HSA.HSAServer;
import SPAYMAPLib.ServerSPAYMAP;
import Server.Manager;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Server {

    protected Manager requestManager;
    protected JDBCRequests BDLink ;
    private HSAServer HSA ; 
    
    protected void setBDLink() throws SQLException, NullPointerException, ClassNotFoundException
    {
        BDLink = new JDBCRequests();
    }

    protected Manager setManager(MySocket socketService) throws NullPointerException, ClassNotFoundException, SQLException
    {
        return (Manager) new ServerSPAYMAP(BDLink,socketService,HSA);
    }
    

    
    public void runServer(int nbThread, int port, String serverName)
    {

        ServerGUI servergui = new ServerGUI(serverName,port);
        servergui.setVisible(true);

        boolean coDB = false;

        while(!coDB) {
            try {
                    setBDLink();
                coDB = true;
            } catch (SQLException | ClassNotFoundException e) {
                servergui.printLog("Connection to database failed retrying in 5 seconds...");
                servergui.printLog(e.getMessage());
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        
        servergui.printLog("Connection to database initialized");
        
        MySocketServer listeningSocket;
        try {
            listeningSocket = new MySocketServer(port,true);
            servergui.printLog("Socket server initialized");
            RunServer(nbThread, listeningSocket, servergui);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
    
    protected void RunServer(int nbThread, MySocketServer listeningSocket, ServerGUI servergui)
   {
        ExecutorService threadPool = Executors.newFixedThreadPool(nbThread);
        ExecutorService AdminPool = Executors.newFixedThreadPool(1);
        HSA = new HSAServer(threadPool,BDLink) ; 
        
        AdminPool.execute(new ThreadAdmin(HSA,servergui));
        servergui.printLog("Thread pool initialized");

        while(true){
            try {
                MySocket clientSocket;
                try {
                    clientSocket = listeningSocket.Accept();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                
                threadPool.execute(new ServerThread(clientSocket, setManager(clientSocket), servergui, HSA));
                servergui.printLog("Connection accepted");
            } catch (NullPointerException | ClassNotFoundException | SQLException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
