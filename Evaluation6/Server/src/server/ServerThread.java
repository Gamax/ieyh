package server;

import HSA.HSAServer;
import MessageLib.Message;
import Server.Manager;
import SocketFacilities.MySocket;

import java.io.IOException;
import java.net.SocketException;

public class ServerThread implements Runnable{

    private MySocket clientSocket;
    private Manager requestManager;
    private ServerGUI serverGUI;
    private HSAServer HSA ;
    
    public ServerThread(MySocket clientSocket, Manager requestManager, ServerGUI serverGUI,HSAServer hsa){
        this.clientSocket = clientSocket;
        this.requestManager = requestManager;
        this.serverGUI = serverGUI;
        HSA = hsa ;
    }

    public void run()
    {
        Message message;
        Message reponse ;
        HSA.addIP(clientSocket.ConnectedTo()); 
        while(true) //test
        {
            
            try {
               message = (Message) clientSocket.ReceiveObj();
               
                serverGUI.printLog("[Thread] RequestReceived [" + message + "]");
                
                reponse = requestManager.Traitement(message);
                if(reponse != null)
                {
                    clientSocket.SendObj(reponse);
                    serverGUI.printLog("[Thread] ResponseSent [" + reponse + "]");
                }
                else
                {
                    clientSocket.SendObj(new Message(-1,"Internal Error"));
                   
                    serverGUI.printLog("[Thread] ResponseSent [internal error]");
                }
            }
            catch (SocketException e){
                serverGUI.printLog("[Thread] Connection Ended");
                HSA.removeIP(clientSocket.ConnectedTo());
                return;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) 
            {
                e.printStackTrace();
                try
                {
                    //TODO : remplacer par socketObject()
                    clientSocket.SendObj(new Message(-1,"Internal Error"));
                    serverGUI.printLog("[Thread] ResponseSent [Internal error !]");
                }
                catch (IOException ex) 
                {
                    ex.printStackTrace();
                }
            }
             
        }
    }
}
