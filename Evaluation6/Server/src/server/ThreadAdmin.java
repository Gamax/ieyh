/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import DBFacilities.JDBCRequests;
import HSA.HSAServer;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import java.io.IOException;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.ServerGUI;

/**
 *
 * @author Steph
 */
public class ThreadAdmin implements Runnable{

    
    private HSAServer HSA ;
    private ServerGUI servergui ;
    public ThreadAdmin(HSAServer value, ServerGUI gui )
    {
        HSA = value ;
        servergui = gui ;
    }
    
    @Override
    public void run() {
        
        MySocketServer socketAdmin ;
        MySocket clientSocket ;
        try {
            socketAdmin = new MySocketServer(50400,false);
        } catch (IOException ex) {
            Logger.getLogger(ThreadAdmin.class.getName()).log(Level.SEVERE, null, ex);
            return ;
        }
        
        String message ;
        while(true)
        {
            try {
                servergui.printLog("[Thread Admin] administrator thread initialized");
                clientSocket = socketAdmin.Accept() ;
                servergui.printLog("[Thread Admin] Administrator connected");
                try {
                    while(true)
                    {
                        message = clientSocket.Receive();
                         servergui.printLog("[Thread Admin] RequestReceived [" + message + "]");
                         if(message.contains("F"))
                         {
                             servergui.printLog("[Thread Admin] Client has left");
                             break ;
                         }
                         message = HSA.traitement(message);
                         if(message != null)
                         {
                             clientSocket.Send(message);
                             servergui.printLog("[Thread Admin] ResponseSent [" + message + "]");
                         }
                         else
                         {
                             clientSocket.Send("N:Internal Error");
                             servergui.printLog("[Thread Admin] ResponseSent [internal error]");
                         }
                    }
                 }
                 catch (SocketException e){
                     servergui.printLog("[Thread Admin] Connection Ended");
                     return;
                 }
                 catch (IOException e) {
                     servergui.printLog("[Thread Admin] Error : " + e.getCause());
                     e.printStackTrace();
                 }
                
            } catch (IOException ex) {
                servergui.printLog("[Thread Admin]  Error : " + ex.getCause());
                Logger.getLogger(ThreadAdmin.class.getName()).log(Level.SEVERE, null, ex);
                return ;
            }
            catch (Exception e){
                servergui.printLog("[Thread Admin] Error : " + e.getCause());
                 Logger.getLogger(ThreadAdmin.class.getName()).log(Level.SEVERE, null, e);
                return ;
            }
        }
    }
    
}
