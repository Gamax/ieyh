/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serveurcard;

import CCAPLib.ServerCCAP;
import DBFacilities.JDBCRequestCard;
import Server.Manager;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.Server;
import server.ServerGUI;

/**
 *
 * @author Steph
 */
public class ServerCard extends Server {
    
    private JDBCRequestCard BDLinkCard ;
    
    @Override
    protected void setBDLink() throws SQLException, NullPointerException, ClassNotFoundException
    {
        BDLinkCard = new JDBCRequestCard();
    }
    
    @Override
    protected Manager setManager(MySocket socketService) throws NullPointerException, SQLException, ClassNotFoundException
    {
        return (Manager) new ServerCCAP(BDLinkCard);
    }
    
    @Override
    protected void RunServer(int nbThread, MySocketServer listeningSocket, ServerGUI servergui)
   {
        ExecutorService threadPool = Executors.newFixedThreadPool(nbThread);

        servergui.printLog("Thread pool initialized");

        while(true){
            MySocket clientSocket;
            try {
                clientSocket = listeningSocket.Accept();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            try {
                ServerThread srvThread = new ServerThread(clientSocket, setManager(clientSocket), servergui);
                threadPool.execute(srvThread);
                servergui.printLog("Connection accepted");
            } catch (NullPointerException | ClassNotFoundException | SQLException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
}
