/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import MessageLib.Message;
import MessageLib.Pay;
import SocketFacilities.MySocket;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thomas
 */
public class TestObjectSocketClient {
    
    public static void main(String[] args) {
        
        Pay objPay = new Pay("42",10,"CARD");
        Message objMessage = new Message(4,objPay);
        
       
            try {
                System.out.println("init Cli Socket");
                MySocket sockcli = new MySocket("0.0.0.0", 50000,true);
                System.out.println("Cli Socket Open");
                sockcli.SendObj(objMessage);
                System.out.println("obj sent");
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(TestObjectSocketClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
    
}
