/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 *
 * @author Thomas
 */
public class Debit implements Serializable{
    private double montant;
    private String creditCard;

    public Debit(double montant, String creditCard) {
        this.montant = montant;
        this.creditCard = creditCard;
    }

    public double getMontant() {
        return montant;
    }

    public String getCreditCard() {
        return creditCard;
    }
    
    @Override
    public String toString() {
        return "Debit{" + "montant=" + montant + ", creditCard=" + creditCard + '}';
    }
    
    
}
