/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 *
 * @author Steph
 */
public class PayPanier implements Serializable{
    private String numCarte ;
    private String Client ; 

    public PayPanier() {
        
    }
    
    public PayPanier(String num, String client)
    {
        numCarte = num ;
        Client = client ;
    }
    
    public void setCarte(String value)
    {
        numCarte = value ;
    }
    
    public void setClient(String value)
    {
        Client = value ;
    }
    
    public String getCarte()
    {
        return numCarte ;
    }
    
    public String getClient()
    {
        return Client ;
    }

    @Override
    public String toString() {
        return "PayPanier{" + "numCarte=" + numCarte + ", Client=" + Client + '}';
    }
    
    
    
}
