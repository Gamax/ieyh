/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

/**
 *
 * @author Steph
 */
public class IPAdmin {
    String ipAdmin ;
    String ipClient ;
    
    
    public IPAdmin()
    {
        ipAdmin = new String();
        ipClient = new String();
    }
    
    public IPAdmin(String ipclient, String ipadmin)
    {
        ipAdmin = ipadmin;
        ipClient = ipclient;
    }
    
    public String getIPAdmin() { return ipAdmin ; }
    public String getIPClient() { return ipClient ; }
    public void setIPAdmin(String value) { ipAdmin = value ; }
    public void setIPClient(String value) { ipClient = value ; }
    
    @Override
    public String toString()
    {
        return "[Client : " + ipClient + " Admin : " + ipAdmin + "]";
    }
}
