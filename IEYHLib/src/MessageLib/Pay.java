/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 *
 * @author Thomas
 */
public class Pay implements Serializable{
    private String id;
    private double montant;
    private String creditCard;

    public Pay(String id, double montant, String creditCard) {
        this.id = id;
        this.montant = montant;
        this.creditCard = creditCard;
    }

    public Pay()
    {
        
    }
    
    public Pay(Pay original)
    {
        id = original.getId();
        montant = original.getMontant();
        creditCard = original.getCreditCard();
    }
    
    public void setID(String value)
    {
        id = value ;
    }
    
    public void setMontant(double value)
    {
        montant = value ;
    }
    
    public void setCreditCard(String value)
    {
        creditCard = value ;
    }
    
    public String getId() {
        return id;
    }

    public double getMontant() {
        return montant;
    }

    public String getCreditCard() {
        return creditCard;
    }

    @Override
    public String toString() {
        return "Pay{" + "id=" + id + ", montant=" + montant + ", creditCard=" + creditCard + '}';
    }
    
    
    
    
}
