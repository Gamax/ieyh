/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Thomas
 */
public class Login implements Serializable{
    private String user;
    private byte[] digestPwd;

    
    public Login()
    {
        Security.addProvider(new BouncyCastleProvider()); //TODO c'est un peu dégueu
    }
    
    public Login(String username, String password, String sel) throws NoSuchAlgorithmException, NoSuchProviderException
    {
        this();
        setUser(username);
        setPassword(password, sel) ;
    }
    public Login(String user, byte [] digestPwd) {
        this();
        this.user = user;
        this.digestPwd = digestPwd;
    }
    
    public void setUser(String username)
    {
        System.out.println("LOGIN USER :" + username);
        user = username ;
    }
    
    public void setPassword(String password, String sel) throws NoSuchAlgorithmException, NoSuchProviderException
    {
        System.out.println("LOGIN PSWD :" + password + sel);
        MessageDigest md = MessageDigest.getInstance("SHA-1","BC");
        md.update(user.getBytes());
        md.update(password.getBytes());
        md.update(sel.getBytes());
        digestPwd = md.digest() ;
    }
    
    public String getUser() {
        return user;
    }

    public byte [] getDigestPwd() {
        return digestPwd;
    }

    @Override
    public String toString() {
        return "Login{" + "user=" + user + ", digestPwd=" + digestPwd + '}';
    }
    
    public boolean isEquals(Login log) {
        System.out.println(getUser()+"/"+log.getUser());
        System.out.println(new String(getDigestPwd()) + "/" + new String(log.digestPwd));
        return getUser() == log.getUser() && MessageDigest.isEqual(digestPwd, log.digestPwd);
    }
}
