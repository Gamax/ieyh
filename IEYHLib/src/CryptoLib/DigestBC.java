/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CryptoLib;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Thomas
 */
public class DigestBC {
    
    String codeProvider;
    MessageDigest md;
    
    public DigestBC() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        Security.addProvider(new BouncyCastleProvider());
        codeProvider = "BC";
        md = MessageDigest.getInstance("SHA-1",codeProvider);
    }
    
    public void update(String input)
    {
        md.update(input.getBytes());
    }
    
    public String digest()
    {
        byte[] tmp = md.digest();
        md.reset();
        return new String(tmp);
    }
    
    public boolean isEqual(String d1,String d2)
    {
        return MessageDigest.isEqual(d1.getBytes(), d2.getBytes());
    }
    
}
