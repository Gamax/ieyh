/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thomas
 */

import SocketFacilities.MyUDPSocket;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
        
public class TestUDP {
    
    
    public static void main(String[] args)
    {
        try {
            MyUDPSocket socket = new MyUDPSocket("230.0.0.1", 5000);
            System.out.println("Socket initialized");
             System.in.read();
            socket.Send("yolo");
            System.out.println("Packet sent");
            System.out.println("Press any key to start receiving");
            System.in.read();
            while(true)
            {
            String msg = socket.Receive();
            
            System.out.println("Packet received");
            
            System.out.println("["+msg+"]");
            }
        } catch (IOException ex) {
            Logger.getLogger(TestUDP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
