/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import CryptoLib.DigestBC;
import java.io.*;
import java.net.*;
import java.util.Date;
import java.security.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class TestDigest {
    public static void main(String[] args){
        /*try {
            Security.addProvider(new BouncyCastleProvider());
            String codeProvider = "BC";
            
            String s = "hellosalut";
            String pwd = "hepl";
            
            MessageDigest md = MessageDigest.getInstance("SHA-1",codeProvider);
            md.update(s.getBytes());
            md.update(pwd.getBytes());
            byte[] digest = md.digest();
            String digestString = new String(digest);
            System.out.println(digestString);
            
            MessageDigest md1 = MessageDigest.getInstance("SHA-1",codeProvider);
            md1.update(s.getBytes());
            md1.update(pwd.getBytes());
            byte[] digest1 = md1.digest();
            String digestString1 = new String(digest1);
            System.out.println(digestString1);
            
            if(MessageDigest.isEqual(digestString.getBytes(), digestString1.getBytes()))
                System.out.println("Succès string !");
            
            if(MessageDigest.isEqual(digest, digest1))
                System.out.println("Succès !");
            
            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TestDigest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(TestDigest.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        DigestBC digestbc;
        try {
            digestbc = new DigestBC();
            digestbc.update("lol1");
            digestbc.update("lol");
            String digest = digestbc.digest();
            digestbc.update("lol1");
            digestbc.update("lol");
            String digest2 = digestbc.digest();
            if(digestbc.isEqual(digest, digest2))
                System.out.println("OK");
        
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TestDigest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(TestDigest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
}
