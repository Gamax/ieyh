/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HSA;

/**
 *
 * @author Steph
 */
public interface EmergencyAdminPort {
    public void setPause();
    public void setStop();
    public void setPortAdmin(String ip);
    public String getPortClient();
    public void quit();
}
