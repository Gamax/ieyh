/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HSA;

import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steph
 */
public class TraitementEmergencyAdmin implements Runnable {
    
    private EmergencyAdminPort Application ;
    
    public TraitementEmergencyAdmin(EmergencyAdminPort application)
    {
        Application = application ;
    }

    @Override
    public void run() {
        try {
            String ipPort = Application.getPortClient() ;
            String ip = ipPort.substring(0,ipPort.indexOf(":"));
            int port = Integer.parseInt(ipPort.substring(ipPort.indexOf(":")+1));
            System.out.println("THREAD ADMIN : "+ip+":"+String.valueOf(port+201));
            Application.setPortAdmin(ip+":"+String.valueOf(port+201));
            MySocketServer socket = new MySocketServer(port+201,false);
            MySocket listeningSocket ;
            String message ;
            while(true)
            {
                try {
                    listeningSocket = socket.Accept();
                    
                    while(true)
                    {
                        message = listeningSocket.Receive();
                        
                        if(message.startsWith("1"))
                            Application.setPause();
                        else if(message.startsWith("2"))
                            Application.setStop();
                        else
                            System.out.println("unknown request");
                    }
                    
                } catch (Exception e) {
                    System.out.println("Error : " + e.getCause() + " : " + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Error : " + e.getCause() + " : " + e.getMessage());
            Application.quit();
        }
        
    }
    
    
    
}
