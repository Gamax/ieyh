/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HSA;

import SocketFacilities.MySocket;
import java.io.IOException;
/**
 *
 * @author Steph
 */
public class HSAClient {
    
    private MySocket socket ;
    
    public HSAClient()
    {
    }
    
    public boolean loginA(String nom, String mdp) throws IOException
    {
        transmission("1:"+nom+","+mdp);
        String reponse = reception();
        
        if(reponse.startsWith("Y"))
            return true ;
        System.out.println(reponse.substring(2));
        return false ;
    }
    
    public String LClients() throws IOException
    {
        transmission ("2");
        
        return reception(); 
    }
    
    
    public boolean pause() throws IOException
    {
        transmission("3");
        
        String reponse = reception () ;
        if(reponse.startsWith("Y"))
            return true ;
        System.out.println(reponse.substring(2));
        return false ;
    }
    
    public boolean stop(int nbSecondes) throws IOException
    {
        transmission("4:"+Integer.toString(nbSecondes));
        
        String reponse = reception () ;
        if(reponse.startsWith("Y"))
            return true ;
        System.out.println(reponse.substring(2));
        return false ;
    }
    
    public void connect(String ip, int port) throws IOException
    {
        socket = new MySocket(ip, port);
    }
    
    public void disconnect() throws IOException
    {
        if(socket.isAlive())
        {
            transmission("F");
            socket.Close(); 
        }
        else
            System.out.println("Socket already closed");        
    }
    
    public void transmission(String msg) throws IOException
    {
        System.out.println("Envoi de ["+msg+"]");
        socket.Send(msg);
    }
    
    public String reception() throws IOException
    {
        return socket.Receive();
    }
}
