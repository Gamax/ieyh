/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HSA;

import DBFacilities.JDBCRequests;
import MessageLib.IPAdmin;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 *
 * @author Steph
 */
public class HSAServer{

    private List<String> IP ;
    private ExecutorService PoolServer ;
    private JDBCRequests BDLink ;
    private HashMap IPMap ;
    
    
    public HSAServer(ExecutorService threadpoolserver, JDBCRequests bdlink)
    {
        IPMap = new HashMap();
        IP = new ArrayList<>();
        BDLink = bdlink ;
    }
    
    public void addIP(String ip) // TODO : retirer quand IP Admin gerees ! 
    {
        IP.add(ip);
    }
    
    public void addIP(IPAdmin ip)
    {
        IPMap.put(ip.getIPClient(), ip.getIPAdmin());
    }
    
    public void removeIP(String ip)
    {
        IP.remove(ip);
        
        IPMap.remove(ip);
    }
    
    public String traitement (String msg)
    {
        char option = msg.charAt(0);
        
        switch(option)
        {
            case '1' : return loginA(msg.substring(2)) ;
            case '2' : return LClients(msg) ;
            case '3' : return Pause(msg) ;
            case '4' : return Stop(msg.substring(2));
            default  : return "N:Unknown request" ;
        }
    }
    
    public String loginA(String msg)
    {
        try {
            String user = msg.substring(0,msg.indexOf(","));
            String password = msg.substring(msg.indexOf(",")+1);
            if(BDLink.CheckPassword("USERADMIN", user, password))
            {
                return "Y";
            }
            return "N:Invalid credentials";
        } catch (SQLException ex) {
            return "N:Database fail : " + ex.getCause() + " : " + ex.getMessage();
        }
    }
    
    public String LClients(String msg)
    {
        try {
            if(IP.isEmpty())
                return "#No client connected"; 
            int cpt = IP.size();
            StringBuilder str = new StringBuilder(); 
            for(int i = 0 ; i < cpt ; i++)
            {
                str.append("#");
                str.append(IPMap.get(IP.get(i)));
            }
            
            if(str == null)
                return "#No client connected";
            return str.toString() ;
        } catch (NullPointerException e) {
            return "#No client connected";
        }
    }
    
    public String Pause(String msg)
    {
        
        return msg ; 
    }
    
    public String Stop(String msg)
    {
        try {
            int nbSecondes = Integer.parseInt(msg);
            System.out.println(nbSecondes);
            Thread.sleep(nbSecondes*1000);
            PoolServer.shutdownNow();
            return "Y:server stopped !" ;
        } catch (NumberFormatException e) {
            return "N:Invalid timelapse !";
        } catch (InterruptedException ex) {
            return "N:Thread won't sleep";
        } catch (NullPointerException e) {
            return "N:Hum... error null pointer ... ";
        }
    }
}
