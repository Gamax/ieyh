/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SPAYMAPLib;
import CCAPLib.ClientCCAP;
import CryptoLib.CryptoManager;
import CryptoLib.SessionCrypto;
import DBFacilities.JDBCRequests;
import Formatage.LectureResultSet;
import HSA.HSAServer;
import MessageLib.* ;
import Server.Manager ;
import SocketFacilities.MySocket;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
/**
 *
 * @author Steph
 */
public class ServerSPAYMAP implements Manager {
    private JDBCRequests BDLink ;
    private Login userEnCours ;
    private String sel ;
    private ClientCCAP cardCheck ;
    private SessionCrypto sessionCrypto;
    private MySocket socketService;
    private MySocket socketCard;
    private CryptoManager cryptoManager;
    private int numFactureEnCours;
    private HSAServer HSA ;
    
    public ServerSPAYMAP(JDBCRequests bdlink, MySocket socketService, HSAServer hsa) throws NullPointerException, SQLException
    {
        BDLink = bdlink;
        HSA = hsa ;
        userEnCours = new Login();
        this.socketService = socketService;
        try {
            cryptoManager = new CryptoManager("../../Evaluation6/keystore/ServPay.p12"); 
        } catch (Exception e) {
            System.err.println("Error loading cryptoManager : "+e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
        
        try {
            socketCard = new MySocket("0.0.0.0", 50210,true);
        } catch (IOException e) {
            System.err.println("Error connecting to ServCard : "+e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }

        cardCheck = new ClientCCAP(socketCard);

        
        
    }
    
    
    
    @Override
    public Message Traitement(Message msg)
    {
        
        try {
            switch(msg.getId())
            {
                case 1  : return SynLogin(msg);
                case 2  : return Login(msg);
                case 4  : return Payer(msg); //requete crypt / reponse crypt
                case 7  : return PayerConfirmation(msg); //requete crypt / reponse non-crypt
                //case 8  : return handshake(msg);
                case 10 : return getReservations(msg); //requete non-crypt / reponse crypt
                case 11 : return getReservationsPar(msg); //requete non-crypt / reponse crypt
                case 12 : return PayerPanier(msg); //requete crypt / reponse crypt
                case 13 : return addAdminPort(msg);
                default : return new Message(-1,"Unknown request !");
            }
        } catch (Exception e) {
            Logger.getLogger(ServerSPAYMAP.class.getName()).log(Level.SEVERE, null, e);
        }
        return new Message(-1, "Internal error !");        
    }
    
    public Message addAdminPort(Message msg)
    {
        try {
            HSA.addIP((IPAdmin)msg.getContent());
            return new Message(0,null);
        } catch (Exception e) {
            return new Message(-1,"Erreur pour passer IP admin");
        }
    }
    
    
    
    public Message SynLogin(Message msg)
    {
        Random generator = new Random();  
        
        Date date = new Date();
        sel = Integer.toString(generator.nextInt(1000)) + date.toString().replaceAll(" ", "_");
        Message reponse = new Message();
        reponse.setID(1);
        reponse.setContenu(sel);
        return reponse ;
    }
    
    public Message Login(Message msg) throws SQLException, NoSuchAlgorithmException, NoSuchProviderException, IOException
    {
        Message reponse = new Message();
        Login log = (Login)msg.getContent();
        String pswd = BDLink.Getpassword("USERAGENCES",log.getUser()) ;
        if (pswd == null)
        {
            reponse.setID(-1);
            reponse.setContenu("User or password incorrect !");
            return reponse ;
        }
        userEnCours.setUser(log.getUser());
        userEnCours.setPassword(pswd, sel);
        System.out.println("userEnCours  : " + userEnCours);
        System.out.println("Connexion de : " + log);
        
        if(userEnCours.isEquals(log))
        {
            reponse.setID(0);
        }
        else
        {
            reponse.setID(-1);
            reponse.setContenu("User or password incorrect !");
        }
        
        socketService.SendObj(reponse); //envoi réponse login
        
        return handshake(); //démarrage immédiat handshake
    }
    
    public Message handshake() // TODO : handshake !
    {
        try // TODO : handshake !
        {
            sessionCrypto = cryptoManager.handshakeServer(socketService, userEnCours.getUser());
            System.out.println("Session Crypto : " + sessionCrypto);
            return getReservations(null);
        } catch (Exception e) {
            System.err.println("Error handshake : "+e.getMessage());
            e.printStackTrace();
            return new Message(-1, null);
        }
    }
    
    public Message getReservations(Message msg) throws SQLException, IOException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException
    {
        Message reponse = new Message();
        ResultSet rs = BDLink.getReservationNonPayees();
        reponse.setID(10);
        reponse.setContenu(cryptoManager.symCrypt(sessionCrypto.getCryptKey(), LectureResultSet.formatageRSToString(rs)));
        return reponse ;
    }
    
    public Message getReservationsPar(Message msg) throws SQLException, IOException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException
    {
        Client client = (Client)msg.getContent();
        Message reponse = new Message();
        ResultSet rs = BDLink.getReservationNonPayeesPar(client.getPrenom(), client.getNom());
        reponse.setID(11);
        reponse.setContenu(cryptoManager.symCrypt(sessionCrypto.getCryptKey(), LectureResultSet.formatageRSToString(rs)));
        return reponse ;
    }
    
    public Message Payer(Message msg) throws SQLException, IOException, ClassNotFoundException, IllegalBlockSizeException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, BadPaddingException, SignatureException, KeyStoreException
    {
        Message reponse = new Message();
       
        
        Pay pay = (Pay) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(), (SealedObject) msg.getContent());
        
        System.out.println("SERVERSPAYMAP Pay : " + msg.getContent().toString());
        
        if(!cryptoManager.verifySignatureEmploye(userEnCours.getUser(), pay.toString(), msg.getSignature())){
            reponse.setID(-1);
            reponse.setContenu("Error : Signature is invalid !");
            return reponse ;
        }
        
        if(!BDLink.checkAmount(pay.getId(),pay.getMontant()))
        {
            reponse.setID(-1);
            reponse.setContenu("Error : specified amount higher than amount to pay !");
            return reponse ;
        }
        
        msg = cardCheck.CheckCard(pay.getCreditCard(), pay.getMontant());
        if(msg.getId() == 0)  
        {
            String idUser = BDLink.getReferantReservation(pay.getId());
            if(idUser!=null)
            {
                try {
                    if(pay.getMontant()<0)
                    {
                        reponse.setID(-1);
                        reponse.setContenu("Error : specified amount cannot be negative !"); 
                        return reponse ;
                    }
                    
                    String resultPay = BDLink.PayReservation(pay.getId(), idUser, pay.getCreditCard(), String.valueOf(pay.getMontant()));
                } catch (SQLException e) {
                    reponse.setID(-1);
                    
                    if(e.getMessage().startsWith("ORA-20202"))
                        reponse.setContenu("Error : specified amount higher than amount to pay !"); 
                    else
                        reponse.setContenu("Error : " + e.getMessage());
                    return reponse ;
                }
                    
                reponse.setID(6);
                
                PayResponse payresp = new PayResponse();
                numFactureEnCours = BDLink.getNumFacture(pay.getId()); //utilisé par hmac
                payresp.setNum(numFactureEnCours);
                
                if(payresp.getNumeroTransac()==-1)
                {
                    reponse.setID(-1);
                    reponse.setContenu("Internal error : get transaction number !"); 
                }
                else
                {
                    payresp.setSolde(BDLink.getSolde(pay.getId()));
                    if(payresp.getSoldeRestant()==-1)
                    {
                        reponse.setID(-1);
                        reponse.setContenu("Internal error : get unpaid amount !"); 
                    }
                    else
                    {
                        reponse.setContenu(cryptoManager.symCrypt(sessionCrypto.getCryptKey(), payresp)); 
                    }
                }
            }
            else
            {
               reponse.setID(-1);
               reponse.setContenu("Internal error !"); 
            }
            
        }
        else
        {
            reponse.setID(-1);
            reponse.setContenu((String)msg.getContent());
        }
        
        return reponse ;

    }
    
    public Message PayerConfirmation(Message msg) throws SQLException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException
    {
        boolean resultat = cryptoManager.verifyHMAC(sessionCrypto.getHmacKey(), userEnCours.getUser(), numFactureEnCours, (byte[]) msg.getContent());
        
        if(resultat)
            return new Message(0,"Hmac validé");
        
        return new Message(-1,"Hmac incorrect");
    }
    
    
    public Message PayerPanier(Message msg) throws SQLException, IOException, ClassNotFoundException, IllegalBlockSizeException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, BadPaddingException, SignatureException, KeyStoreException
    {
        Message reponse = new Message();
              
        
        
        PayPanier pay = (PayPanier) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(), (SealedObject) msg.getContent());
        
        if(!cryptoManager.verifySignatureEmploye(userEnCours.getUser(), pay.toString(), msg.getSignature())){
            reponse.setID(-1);
            reponse.setContenu("Error : Signature is invalid !");
            return reponse ;
        }
        
        
        System.out.println("SERVERSPAYMAP Panier : " + pay.toString());
            
        double montant = BDLink.GetPanierPrix(pay.getClient());
        if(montant == 0)
            return (new Message(-1,"Erreur lors de la récupération du panier !"));
        msg = cardCheck.CheckCard(pay.getCarte(), montant);
        if(msg.getId() == 0)  
        {
            ResultSet facture = BDLink.payPanier(pay.getClient(), pay.getCarte());
            if(facture!= null)
            {
                facture.next();
                
                numFactureEnCours = BDLink.getNumFacture(facture.getNString("IDENTIFIANT"));
                
                PayResponse payResponse = new PayResponse(0,numFactureEnCours);
                
                return (new Message(6,cryptoManager.symCrypt(sessionCrypto.getCryptKey(), payResponse)));
            }
            else
                return (new Message(-1,"Impossible de payer !"));
        }
        else
            return msg;
                
    }
}
