/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package SPAYMAPLib;
import CryptoLib.CryptoManager;
import CryptoLib.SessionCrypto;
import MessageLib.* ;
import SocketFacilities.MySocket;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;

/**
 *
 * @author Steph
 */
public class ClientSPAYMAP {
    
    private CryptoManager cryptoManager;
    private MySocket socketService;
    private SessionCrypto sessionCrypto;
    
    
    private Message msg ;
    private Login login ;
    public ClientSPAYMAP(String keyStorePath)
    {
        /*
        try {
        SRVLink = new ServerSPAYMAP(new JDBCRequests());
        //TODO : init crypto lib
        } catch (NullPointerException | SQLException | ClassNotFoundException ex) {
        System.out.println("Erreur création SRVLink : " + ex.getCause() + " : " + ex.getMessage());
        }*/
        try {
            cryptoManager = new CryptoManager(keyStorePath);
            socketService = new MySocket("0.0.0.0", 50200,true); //todo hardcoded
        } catch (Exception e) {
            System.out.println("Exception : " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
        
        
        
    }
    
    public boolean authenticate(String username, String password) throws NoSuchAlgorithmException, NoSuchProviderException
    {
        msg = new Message();
        
        msg.setID(1); //SYNLOGIN
        msg.setContenu(null);
        
        
        try {
            transmission(msg);
            msg = reception();
            login = new Login(username, password, (String)msg.getContent());
        } catch (Exception e) {
            System.out.println ("Erreur lors du SYSLOGIN : "+e.getCause() + " : " + e.getMessage());
            System.exit(1);
        }
        
        
        msg.setID(2); //LOGIN
        msg.setContenu(login);
        
        try {
            transmission(msg);
            msg = reception();
            
            if(msg.getId() == 0) //OK
                return true ;
            else                 //NOK
            {
                System.out.println(msg.getContent());
                return false ; //TODO : verifier pourquoi le digest bug !!!
            }
        } catch (Exception ex) {
            System.out.println("Impossible de s'authentifier ! " + ex.getCause() + " : " + ex.getMessage());
            System.exit(1);
        }
        return false;
    }
    
    public String handshake() 
    {
        try {
            
            
            sessionCrypto = cryptoManager.handshakeClient(socketService, login.getUser());
            
            System.out.println("Session Crypto : " + sessionCrypto);
            
            Message message = reception();
            
            if(message.getId() == -1){
                System.err.println("Error handshake : return -1");
                System.exit(1);
            }
            
            return (String) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(), (SealedObject) message.getContent());
        } catch (Exception e) {
            System.err.println("Error handshake : "+e.getMessage());
            System.exit(1);
        }
        return null;
        
    }
    
    public String getAllReservations() throws IOException, ClassNotFoundException
    {
        msg.setID(10);
        msg.setContenu(null);
        try {
            transmission(msg);
        } catch (IOException e) {
            return "Erreur de transmission : " + e.getMessage();
        }
        
        msg = reception();
        
        try {
            return (String) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(),(SealedObject) msg.getContent());
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | NoSuchProviderException ex) {
            return "Erreur de réception : " + ex.getMessage();
        }
        
    }
    
    public String giveAdminPort(String ipAdmin)
    {
        
        try {
            Message reponse ;
            msg.setID(13);
            IPAdmin msgContent = new IPAdmin(socketService.getAddress(),ipAdmin);
            msg.setContenu(msgContent);
            transmission(msg);
            reponse = reception();
            if(reponse.getId() == 0)
                return "port admin envoye !";
            else
                return (String)reponse.getContent();
        } catch (Exception e) {
            return "Error while sending admin port";
        }
    }
    
    public String getClientPort()
    {
        return socketService.getAddress();
    }
    
    public String getReservationsPar(String prenom, String nom) throws IOException, ClassNotFoundException
    {
        msg.setID(11);
        Client client = new Client(prenom,nom);
        
        msg.setContenu(client);
        try {
            transmission(msg);
        } catch (IOException e) {
            return "Erreur";
        }
        
        msg = reception();
        
        try {
            return (String) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(),(SealedObject) msg.getContent());
        } catch (IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | NoSuchProviderException ex) {
            return "Erreur de réception : " + ex.getMessage();
        }
    }
    
    public String payer(String reservation, String carte, String Montant) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, ClassNotFoundException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, KeyStoreException, UnrecoverableKeyException, SignatureException
    {
        
        Pay pay = new Pay();
        
        pay.setID(reservation);
        pay.setMontant(Double.parseDouble(Montant));
        pay.setCreditCard(carte);
        
        msg.setID(4);
        SealedObject cryptedObject = cryptoManager.symCrypt(sessionCrypto.getCryptKey(), pay);
        msg.setContenu(cryptedObject);
        msg.setSignature(cryptoManager.genSignatureEmploye(login.getUser(), pay.toString()));
        
        try {
            transmission(msg);
        } catch (Exception e) {
            return "Erreur";
        }
        
        //reception payresponse
        msg = reception();
        
        System.out.println(msg.getContent());
        if(msg.getId()==6)
        {
            PayResponse payresponse = (PayResponse) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(), (SealedObject) msg.getContent());
            String str = "Transaction " + payresponse.getNumeroTransac() + " Solde restant : " + payresponse.getSoldeRestant() + " €";
            
            msg.setID(7);
            
            msg.setContenu(cryptoManager.genHMAC(sessionCrypto.getHmacKey(), login.getUser(), payresponse.getNumeroTransac())); //mettre hmac
            try {
            transmission(msg);
            } catch (Exception e) {
            return "Error occured, try again later !";
            }
            
            //reception ok après hmac
            msg = reception();
            
            if(msg.getId() == 0)
            return str ;
            else
            return (String) msg.getContent();
        }
        else
        {
            if(msg.getContent().getClass()=="".getClass())
            {
                return (String)msg.getContent();
            }
            return "Error occured, try again later !";
        }
    }
    
    public String payerPanier(String client, String carte) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, ClassNotFoundException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, BadPaddingException, KeyStoreException, UnrecoverableKeyException, SignatureException
    {
        msg.setID(12);
        
        PayPanier payPanier = new PayPanier(carte, client);
        
        msg.setContenu(cryptoManager.symCrypt(sessionCrypto.getCryptKey(), payPanier));
        msg.setSignature(cryptoManager.genSignatureEmploye(login.getUser(), payPanier.toString()));
        try {
            transmission(msg);
        } catch (Exception e) {
            return "Erreur";
        }
        
        msg = reception();
        
        if(msg.getId() == 6){
        
        PayResponse payResponse = (PayResponse) cryptoManager.symDecrypt(sessionCrypto.getCryptKey(), (SealedObject) msg.getContent());
        
         msg.setID(7);
            
            msg.setContenu(cryptoManager.genHMAC(sessionCrypto.getHmacKey(), login.getUser(), payResponse.getNumeroTransac())); //mettre hmac
            try {
            transmission(msg);
            } catch (Exception e) {
            return "Error occured, try again later !";
            }
            
            //reception ok après hmac
            msg = reception();
            
            if(msg.getId() == 0)
            return "Paiement validé avec succès !";
            else
            return (String) msg.getContent();
        
        
        }
        else
        {
            if(msg.getContent().getClass()=="".getClass())
            {
                return (String)msg.getContent();
            }
            return "Error occured, try again later !";
        }
    }
    
    private void transmission(Message msg) throws IOException
    {
        System.out.println("Transmission : " + msg);
        socketService.SendObj(msg);
    }
    
    private Message reception() throws IOException, ClassNotFoundException
    {
        Message msg = (Message) socketService.ReceiveObj();
        System.out.println("Reception : "+msg);
        return msg;
    }
}
