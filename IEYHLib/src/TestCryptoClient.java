
import CryptoLib.CryptoManager;
import CryptoLib.CryptoManagerException;
import CryptoLib.SessionCrypto;
import SocketFacilities.MySocket;
import MessageLib.*;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
 *
 * @author Thomas
 */
public class TestCryptoClient {
    public static void main(String[] args) {
        try {
            
            CryptoManager cryptoManagerCli = new CryptoManager("../Evaluation6/keystore/AppPay.p12");
            
            System.out.println("Clé CCAP : "+cryptoManagerCli.getCCAPKeyName("servpay"));
            
            System.out.println("init Cli Socket");
            MySocket sockcli = new MySocket("0.0.0.0", 50000,true);
            System.out.println("Cli Socket Open");
            
            SessionCrypto session = cryptoManagerCli.handshakeClient(sockcli, "thomas");
            System.out.println("Recu" + session);
            
            sockcli.SendObj(new Message(0,cryptoManagerCli.symCrypt(session.getCryptKey(), new Client("Thomas","Dethier"))));
            
            Message message;
            int numTransac = 12;
            //HMAC Test
            message = new Message(0,cryptoManagerCli.genHMAC(session.getHmacKey(),session.getUser(),numTransac));
            
            sockcli.SendObj(message);
            
            Pay pay = new Pay("RESERV102345", 120, "164943354896");
            
            //Signature
            message = new Message(0,cryptoManagerCli.symCrypt(session.getCryptKey(), pay),cryptoManagerCli.genSignatureEmploye("thomas", pay.toString()));
            sockcli.SendObj(message);
            
            
        } catch (KeyStoreException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CryptoManagerException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(TestCryptoClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
