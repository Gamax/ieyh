/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketFacilities;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 *
 * @author Thomas
 */
public class MyUDPSocket {
    private InetAddress addressGroup;
    private int port;
    MulticastSocket socketGroup;
    
    public MyUDPSocket(String address, int port) throws IOException
    {
        addressGroup = InetAddress.getByName(address);
        socketGroup = new MulticastSocket(port);
        socketGroup.joinGroup(addressGroup);
        this.port = port;
    }
    
    public void Send(String msg) throws IOException
    {
        DatagramPacket datagram = new DatagramPacket(msg.getBytes("UTF-8"),(msg.getBytes("UTF-8")).length,addressGroup,port);
        socketGroup.send(datagram);
    }
    
    public String Receive() throws IOException
    {
        byte[] buffer = new byte[2000];
        DatagramPacket datagram = new DatagramPacket(buffer, buffer.length);
        socketGroup.receive(datagram);
        return (new String(datagram.getData(),0,datagram.getLength(),"UTF-8"));
    }
    
    public void Close() throws IOException
    {
        socketGroup.leaveGroup(addressGroup);
    }
    
}
