package SocketFacilities;


import java.io.IOException;
import java.net.ServerSocket;

public class MySocketServer{

    private ServerSocket legacyServerSocket;
    private boolean modeObj = false;

    public MySocketServer() throws IOException
    {
        legacyServerSocket = new ServerSocket(20000);
    }

    public MySocketServer(int port) throws IOException
    {
        legacyServerSocket = new ServerSocket(port);
    }
    
    public MySocketServer(int port,boolean modeObj) throws IOException
    {
        this.modeObj = modeObj;
        legacyServerSocket = new ServerSocket(port);
    }

    public MySocket Accept() throws IOException
    {
        return new MySocket(legacyServerSocket.accept(),modeObj);
    }

    public void Close() throws IOException{
        legacyServerSocket.close();
    }
}
