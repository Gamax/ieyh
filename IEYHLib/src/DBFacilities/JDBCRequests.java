package DBFacilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;



public class JDBCRequests {
    
    private JDBCInit BDLink ;
    
    // <editor-fold defaultstate="collapsed" desc="Constructor">
    
    public JDBCRequests () throws NullPointerException, SQLException, ClassNotFoundException
    {
        JDBCInit.LoadOracleDriver();
        BDLink = new JDBCInit();
        BDLink.ConnectOracle("localhost", "1521", "orcl", "test", "oracle");
    }
    
    public JDBCRequests (String user, String password) throws NullPointerException, SQLException, ClassNotFoundException
    {
        JDBCInit.LoadOracleDriver();
        BDLink = new JDBCInit();
        BDLink.ConnectOracle("localhost", "1521", "orcl", user, password);
    }
    
    public JDBCRequests(String BD) throws NullPointerException, SQLException, ClassNotFoundException
    {
        if(BD.toUpperCase().equals("MYSQL") )
        {
            JDBCInit.LoadMySQLDriver();
            // TODO : hardcoder donnees db
            // BDLink = new JDBCInit();
            // BDLink.ConnectMySQL("localhost", port, service, user, password);
        }
        else
        {
            JDBCInit.LoadOracleDriver();
            BDLink = new JDBCInit();
            BDLink.ConnectOracle("localhost", "1521", "orcl", "test", "oracle");
        }
    }
    
    public JDBCRequests(String BD,String address, String port, String service, String user, String password) throws NullPointerException, SQLException, ClassNotFoundException
    {
        if(BD.toUpperCase().equals("MYSQL") )
        {
            JDBCInit.LoadMySQLDriver();
            BDLink = new JDBCInit();
            BDLink.ConnectMySQL(address, port, service, user, password);
        }
        else
        {
            JDBCInit.LoadOracleDriver();
            BDLink = new JDBCInit();
            BDLink.ConnectOracle(address, port, service, user, password);
        }
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Generic">
    public String updateTable(String table, String update) throws SQLException
    {
        try
        {
            BDLink.ExecuteUpdate("UPDATE TABLE " + table + " " + update);
            return "Update succeeded";
        }
        catch(SQLException e)
        {
            return e.getMessage();
        }
    }
    
    public String insertTable(String table, String value) throws SQLException
    {
        BDLink.ExecuteUpdate("INSERT INTO TABLE " + table + " Values (" + value +")");
        return table + " insere(e)";
        
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User">
    
    // <editor-fold defaultstate="collapsed" desc="User - WebUser">
    
    
    //check pwd password site web user
    
    public boolean checkPasswordUser(String Login, String mdp) throws SQLException
    {
        ResultSet rs ;
        rs = BDLink.ExecuteQuery("SELECT * FROM UserClient WHERE username = '" + Login + "' AND password = '" + mdp + "'");
        return rs.next();
    }
    
    //add web user
    
    public void addUser(String login, String mdp, String idClient) throws SQLException
    {
        BDLink.ExecuteUpdate("INSERT INTO UserClient VALUES('"+ login + "','" + mdp + "','" + idClient + "')");
    }
    
    //retourne numero client pour un web user
    
    public String getUserClientID(String username) throws SQLException
    {
        ResultSet set = BDLink.ExecuteQuery("SELECT CLIENT FROM USERCLIENT WHERE USERNAME='"+username+"'");
        if(set.next())
            return set.getString(1);
        else
            return null ;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User - Client">
    
    //Ajoute un client dans la base de donnees et retourne son ID, si le client est deja cree retourne son ID
    
    public String addClientID(String Nom,String Prenom,String Address, String Nationalite,String Naissance, String Mail, String Referant) throws SQLException
    {
        String id = getClientID(Nom,Prenom,Address,Nationalite,Naissance,Mail,Referant);
        if(id!=null)
            return id ;
        ResultSet set = BDLink.ExecuteQuery("SELECT count(*) FROM CLIENTS"); //recup  l'id
        set.next();
        id = Integer.toString((set.getInt(1)+1));
        
        if( BDLink.ExecuteUpdate("INSERT INTO Clients values("+ id +",'"+ Nom +"','"+ Prenom +"','"+ Address +"','"+ Nationalite +"',to_date('"+ Naissance +"','DD/MM/YYYY'),'"+ Mail +"',"+ Referant +")") > 0)
            return id;
        else
            return null;
    }
    
    public String getClientID(String Nom,String Prenom,String Address, String Nationalite,String Naissance, String Mail, String Referant) throws SQLException
    {
        String str = "SELECT * FROM CLIENTS WHERE NOM='"+ Nom+"' AND PRENOM='" + Prenom + "' AND ADRESSE = '" + Address +"' AND NATIONALITE='" + Nationalite + "' AND Naissance='" + Naissance+"' AND Mail='"+Mail+"' AND REFERANT='"+Referant+"'" ;
        ResultSet rs = BDLink.ExecuteQuery(str);
        if(rs.next())
            return rs.getString("ID");
        else
            return null ;
    }
    
    public String getClientMail(String id) throws SQLException
    {
        String str ;
        ResultSet rs = BDLink.ExecuteQuery("SELECT MAIL FROM CLIENTS WHERE ID='"+"id"+"'");
        
        if(rs.next())
            return rs.getString("MAIL");
        else
            return null ;
    }
    
    public ResultSet GetAllClients() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT * FROM CLIENTS");
    }
    
    public ResultSet GetAllClientsName(String name) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT" + name + "FROM CLIENTS");
    }
    
    public ResultSet GetClientName(String name) throws  SQLException
    {
        return BDLink.ExecuteQuery("SELECT prenom FROM clients WHERE prenom = '" + name +"'");
    }
    
    public ResultSet getClient(String prenom, String nom) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT * FROM CLIENTS WHERE NOM='" + nom + "' AND PRENOM='" + prenom + "'");
    }
    
    public ResultSet getClientID(String prenom, String nom) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT ID FROM CLIENTS WHERE NOM='" + nom + "' AND PRENOM='" + prenom + "'");
    }
    
    public ResultSet getClientByID(String id) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Prenom, Nom from CLIENTS WHERE id =" + id );
    }
    
    public ResultSet getClientRef(String IDReservation) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT REFERANT FROM RESERVATION WHERE IDENTIFIANT='"+IDReservation+"'");
    }
    
    public String addClient(String Nom,String Prenom,String Address, String Nationalite,String Naissance, String Mail, String Referant) throws SQLException
    {
        ResultSet set = BDLink.ExecuteQuery("SELECT count(*) FROM CLIENTS"); //recup  l'id
        set.next();
        
        if( BDLink.ExecuteUpdate("INSERT INTO Clients values("+ (set.getInt(1)+1) +",'"+ Nom +"','"+ Prenom +"','"+ Address +"','"+ Nationalite +"',to_date('"+ Naissance +"','DD/MM/YYYY'),'"+ Mail +"',"+ Referant +")") > 0)
            return "Client Added";
        else
            return "Cannot add client to DB !";
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User - ActivitesManager - user villages">
    
    public ResultSet GetAllVendorsName() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT name FROM userVillages");
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User - ChambresManager - user agences">
    
    public ResultSet GetAllAgentsName() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT name FROM userAgences");
    }
    
    // </editor-fold>
    
    //checkpassword fro agences user or villages user dependind of TabLog
    public boolean CheckPassword(String TabLog,String user,String password) throws SQLException
    {
        ResultSet rs ;
        rs = BDLink.ExecuteQuery("SELECT * FROM " + TabLog +" WHERE login = '" + user + "' AND password = '" + password + "'");
        return rs.next();
    }
    
    public String Getpassword(String TabLog,String user) throws SQLException
    {
        ResultSet rs ;
        rs = BDLink.ExecuteQuery("SELECT password FROM " + TabLog +" WHERE login = '" + user + "'");
        if(rs.next()){
            return rs.getString(1);
        }
        else
        {
            return null;
        }
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Chambres">
    
    public ResultSet GetAllChambres() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT * FROM CHAMBRES");
    }
    
    public ResultSet getChambreDate(String NOption) throws SQLException
    {
        String Select = "SELECT Identifiant,Prenom||' '||Nom AS \"Client\", Chambre, to_char(Debut,'DD/MM/YYYY') AS \"Date de Debut\", to_char(Fin,'DD/MM/YYYY') AS \"Date de Fin\", prixNet-NVL(Montant,0)  as \"RESTE\", Paye from contenuFacture full join (SELECT * FROM CLIENTS INNER JOIN RESERVATION ON CLIENTS.ID = RESERVATION.REFERANT) on identifiant = contenuFacture.reservation";
        
        switch(NOption)
        {
            case "1" :  return BDLink.ExecuteQuery(Select + " WHERE Debut <= CURRENT_DATE  AND Fin >= CURRENT_DATE ORDER BY DEBUT,FIN");
            case "2" :  return BDLink.ExecuteQuery(Select + " WHERE Debut > CURRENT_DATE ORDER BY DEBUT,FIN");
            case "3" :  return BDLink.ExecuteQuery(Select + " WHERE PAYE ='N' ORDER BY DEBUT,FIN");
            case "4" :  return BDLink.ExecuteQuery(Select + " ORDER BY DEBUT,FIN");
            default  :  return null;
        }
    }
    
    public ResultSet getChambreCategorie(String categorie) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT * FROM CHAMBRES WHERE CATEGORIE='"+categorie+"' ORDER BY TYPECHAMBRE");
    }
    
    public ResultSet getCategorieChambre() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT CATEGORIE FROM CHAMBRES ORDER BY CATEGORIE");
    }
    
    public ResultSet getTypeChambre() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT TypeChambre FROM CHAMBRES ORDER BY TYPECHAMBRE");
    }
    
    public ResultSet getChambresParCategorieType(String categorie, String type) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT ID, PRIX, TAILLE AS \"NOMBRE DE PERSONNE\", EQUIPEMENT FROM CHAMBRES WHERE CATEGORIE ='" + categorie +"' AND TYPECHAMBRE ='" + type +"' ORDER BY PRIX");
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Activites">
    
    public ResultSet GetAllActivities() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Categorie, Nom, Prix, Places AS \"Nombre de Place\", to_char(DateDebut, 'dd/mm/yyyy') AS \"Date de debut\", to_char(DateFin, 'dd/mm/yyyy') AS \"Date de fin\",ID FROM ACTIVITES ORDER BY Categorie,DateDebut,Prix");
    }
    
    public ResultSet GetAllActiviteReservee() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT * FROM ACTIVITERESERVEE");
    }
    
    public ResultSet GetActivitesCatNom() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT Categorie, Nom FROM activites ORDER BY Categorie, Nom");
    }
    
    public ResultSet GetActivitesNom() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT  Nom FROM activites ORDER BY Nom");
    }
    
    public ResultSet GetAllActiviteNomDate() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Nom, to_char(DateDebut, 'yyyy-mm-dd'), to_char(DateFin, 'yyyy-mm-dd') FROM ACTIVITES ORDER BY Categorie,DateDebut,Prix");
    }
    public ResultSet GetAllActivitesFuture() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Categorie, Nom, Prix, Places AS \"Nombre de Place\", to_char(DateDebut, 'dd/mm/yyyy') AS \"Date de debut\", to_char(DateFin, 'dd/mm/yyyy') AS \"Date de fin\" FROM ACTIVITES WHERE DateDebut >= CURRENT_DATE ORDER BY Categorie,DateDebut,Prix");
    }
    
    public ResultSet GetAllActivitesFutureNomDate() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Nom,to_char(DateDebut, 'dd/mm/yyyy') AS 'Date de debut', to_char(DateFin, 'dd/mm/yyyy') AS 'Date de fin' FROM ACTIVITES WHERE DateDebut >= CURRENT_DATE ORDER BY Categorie,DateDebut,Prix");
    }
    
    public ResultSet GetAllActivitesFutureCatNom() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Categorie,Nom FROM activites WHERE DateDebut >= CURRENT_DATE ORDER BY Categorie,DateDebut,Prix");
    }
    public ResultSet GetAllActivitesCategories () throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT Categorie FROM Activites");
    }
    
    public ResultSet GetActivitesNomFromCat(String cat) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT Nom FROM Activites WHERE UPPER(categorie) = UPPER('" + cat + "')");
    }
    public ResultSet GetActivitesFromCat(String cat) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Categorie, Nom, Prix, Places AS \"Nombre de Place\", to_char(DateDebut, 'dd/mm/yyyy') AS \"Date de debut\",( DateFin - DateDebut + 1) AS \"Duree en jours\" FROM ACTIVITES WHERE DateDebut >= CURRENT_DATE AND UPPER(categorie) = UPPER('" + cat + "') ORDER BY Nom,DateDebut,Prix");
    }
    
    public ResultSet GetActivitesFromOptions(String options) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Categorie, Nom, Prix, Places AS \"Nombre de Place\", to_char(DateDebut, 'dd/mm/yyyy') AS \"Date de debut\",( DateFin - DateDebut + 1) AS \"Duree en jours\",ID FROM ACTIVITES WHERE " + options + " ORDER BY Categorie,Nom,DateDebut,Prix");
    }
    
    public ResultSet GetActivitesNomFromOptions(String options) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT Nom FROM ACTIVITES WHERE " + options + " ORDER BY Nom");
    }
    
    public ResultSet GetActivitesCatFromOptions(String options) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT Categorie FROM ACTIVITES WHERE " + options + " ORDER BY Categorie");
    }
    
    public ResultSet GetActiviteReserveeFromOption(String options) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT * FROM ACTIVITERESERVEE WHERE " + options );
    }
    
    public ResultSet GetActiviteReserveeNom() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Client FROM ACTIVITERESERVEE");
    }
    
    public ResultSet GetActiviteReserveeNomClient(String activite,String client) throws  SQLException
    {
        return BDLink.ExecuteQuery("SELECT client FROM activitereservee WHERE client = '"+client+"' AND activite = "+activite);
    }
    
    
    public ResultSet GetActiviteReserveeNomFromOption(String options) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT Client FROM ACTIVITERESERVEE WHERE " + options);
    }
    
    public String InsciptionActivite(String ID, String nom) throws SQLException
    {
        String str = "INSERT INTO ACTIVITERESERVEE values('" + ID + "','" + nom +"')" ;
        if(BDLink.ExecuteUpdate(str)>0)
            return nom + " inscrit(e)";
        else
            return "impossible de trouver le(la) client(e) !";
    }
    
    public String DesistementActivite(String ID, String nom) throws SQLException
    {
        String str = "DELETE FROM ACTIVITERESERVEE WHERE activite='" + ID + "' AND client='" + nom + "'" ;
        if(BDLink.ExecuteUpdate(str)>0)
            return nom + " desincrit(e)";
        else
            return "Pas de client(e) : " + nom + "inscrit(e)" ;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Reservations">
    
    public boolean testReservationPossible(String chambre, String DateDebut, String DateFin) throws SQLException, ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar debut = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        Date date = new Date();
        date=sdf.parse(DateDebut);
        debut.setTime(date);
        if(debut.before(today))
            return false ;
        String intervalle1 = "to_date('"+DateDebut+" 14:00','DD/MM/YYYY  hh24:MI') BETWEEN DEBUT AND FIN";
        String intervalle2 ="to_date('"+DateFin+" 12:00','DD/MM/YYYY  hh24:MI') BETWEEN DEBUT AND FIN";
        String intervalle3 ="DEBUT BETWEEN  to_date('"+DateDebut+" 14:00','DD/MM/YYYY hh24:MI') AND to_date('"+DateFin+" 12:00','DD/MM/YYYY hh24:MI')";
        String intervalle4 ="FIN BETWEEN  to_date('"+DateDebut+" 14:00','DD/MM/YYYY hh24:MI') AND to_date('"+DateFin+" 12:00','DD/MM/YYYY hh24:MI')";
        
        String requeteImbriquee = "( SELECT CHAMBRE FROM RESERVATION WHERE CHAMBRE='"+chambre+"' AND ( "+intervalle1+" OR "+intervalle2+" OR "+intervalle3+" OR "+intervalle4+"))";
        ResultSet rs = BDLink.ExecuteQuery("SELECT * FROM CHAMBRES WHERE ID IN " + requeteImbriquee );
        if(rs.next())
            return false;
        else
            return true;
    }
    
    public boolean TestClientReservation(String Client, String DateDebut, String DateFin) throws SQLException, ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar debut = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        Date date = new Date();
        date=sdf.parse(DateDebut);
        debut.setTime(date);
        if(debut.before(today))
            return false ;
        String intervalle1 = "to_date('"+DateDebut+" 14:00','DD/MM/YYYY  hh24:MI') BETWEEN DEBUT AND FIN";
        String intervalle2 ="to_date('"+DateFin+" 12:00','DD/MM/YYYY  hh24:MI') BETWEEN DEBUT AND FIN";
        String intervalle3 ="DEBUT BETWEEN  to_date('"+DateDebut+" 14:00','DD/MM/YYYY hh24:MI') AND to_date('"+DateFin+" 12:00','DD/MM/YYYY hh24:MI')";
        String intervalle4 ="FIN BETWEEN  to_date('"+DateDebut+" 14:00','DD/MM/YYYY hh24:MI') AND to_date('"+DateFin+" 12:00','DD/MM/YYYY hh24:MI')";
        
        String requeteImbriquee = "( SELECT CHAMBRE FROM RESERVATION WHERE REFERANT='"+Client+"' AND ( "+intervalle1+" OR "+intervalle2+" OR "+intervalle3+" OR "+intervalle4+"))";
        ResultSet rs = BDLink.ExecuteQuery("SELECT * FROM CHAMBRES WHERE ID IN " + requeteImbriquee );
        if(rs.next())
            return false;
        else
            return true;
    }
    
    
    public String addReservation(String Client, String Chambre, String DateDebut, String DateFin) throws SQLException, ParseException
    {
        if(!testReservationPossible(Chambre,DateDebut,DateFin))
        {
            return "Dates invalides !";
        }
        if(!TestClientReservation(Client,DateDebut,DateFin))
        {
            return "Le client a déjà réservé une chambre à cette période !";
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");
        Calendar debut = Calendar.getInstance();
        Date date = new Date();
        date=sdf.parse(DateDebut);
        debut.setTime(date);
        
        String id = Integer.toString(debut.get(Calendar.YEAR)) + Integer.toString(debut.get(Calendar.MONTH)+1) + Integer.toString(debut.get(Calendar.DAY_OF_MONTH))  + "-"+Chambre + "-"+Client;
        
        BDLink.ExecuteUpdate("INSERT INTO RESERVATION VALUES('" + id  + "','" + Client + "','" + Chambre+"',to_date('"+DateDebut+"','DD/MM/YYYY'),to_date('"+DateFin+"','DD/MM/YYYY'), 0,'N')");
        double prix1 = GetPrixChambre(Chambre);
        int nbJours = GetNbJours(id);
        double prix =  prix1 * nbJours;
        if(BDLink.ExecuteUpdate("UPDATE RESERVATION SET PRIXNET="+prix+" WHERE IDENTIFIANT='"+id+"'")>0)
        {
            System.out.println("ADD RESERVATION : " + Client);
            return Client;
        }
        else
        {
            System.out.println("ADD RESERVATION : null");
            return null ;
        }
    }
    
    public ResultSet getReservation(String client) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT IDENTIFIANT FROM RESERVATION WHERE REFERANT = '" + client + "' AND PAYE = 'N'");
    }
    
    
    public int GetNbJours(String reservation) throws SQLException
    {
        ResultSet rs = BDLink.ExecuteQuery("SELECT FIN-DEBUT AS \"NOMBRE DE JOURS\" FROM RESERVATION WHERE IDENTIFIANT = '"+reservation+"'");
        if(rs.next())
            return rs.getInt(1);
        else
            return -1 ;
    }
    
    public int GetPrixChambre(String chambre) throws SQLException
    {
        ResultSet rs = BDLink.ExecuteQuery("SELECT PRIX FROM CHAMBRES WHERE ID = '"+chambre+"'");
        if(rs.next())
            return rs.getInt(1);
        else
            return -1 ;
    }
    
    public int GetPrixReservation(String reservation) throws SQLException
    {
        ResultSet rs = BDLink.ExecuteQuery("SELECT PRIXNET FROM RESERVATION WHERE IDENTIFIANT = '"+reservation+"'");
        if(rs.next())
            return rs.getInt(1);
        else
            return -1 ;
    }
    
    public ResultSet GetAllReservations() throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT IDENTIFIANT, REFERANT, CHAMBRE, to_char(DEBUT, 'dd/mm/yyyy') AS \"DATE DE DEBUT\", to_char(FIN, 'dd/mm/yyyy') AS \"DATE DE FIN\", PRIXNET AS PRIX, PAYE FROM RESERVATION");
    }
    
    public ResultSet GetAllReservationsFrom(String client) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT IDENTIFIANT, REFERANT, CHAMBRE, to_char(DEBUT, 'dd/mm/yyyy') AS \"DATE DE DEBUT\", to_char(FIN, 'dd/mm/yyyy') AS \"DATE DE FIN\", PRIXNET AS PRIX FROM RESERVATION WHERE REFERANT = '"+client+"' AND PAYE='N'");
    }
    
    public String CancelReservation(String Chambre, String Client)
    {
        try {
            if(BDLink.ExecuteUpdate("DELETE FROM RESERVATION WHERE Referant='"+Client+"' AND CHAMBRE='"+Chambre+"' AND Debut>CURRENT_DATE") >0 )
                return "Annulation effectuee !";
            else
                return "Impossible d'annuler la reservation : date limite d'annulation depassee !";
        } catch (SQLException ex) {
            return "Impossible d'annuler la reservation : un versement a déjà été enregistré pour cette réservation !";
        }
    }
    
    public String PayReservation(String Reservation, String Client, String Credit, String Montant) throws SQLException 
    {
        
        ResultSet rs = BDLink.ExecuteQuery("SELECT * FROM CONTENUFACTURE WHERE RESERVATION='"+Reservation+"'");
        if(rs.next())
            return PayReservation(Reservation, rs.getNString("NUMFACTURE"), Double.parseDouble(Montant));
        else
        {
            String facture = CreateFacture(Client, Credit) ; 
            return PayReservation(Reservation, facture , Double.parseDouble(Montant));
        }
    }
    
    public String PayReservation(String reservation, String facture, double Montant) throws SQLException
    {
        if(Montant < 0)
            return "Pas de remboursement via ce service !";
        ResultSet rs = BDLink.ExecuteQuery("SELECT * FROM CONTENUFACTURE WHERE RESERVATION ='"+reservation+"' AND NUMFACTURE='"+facture+"'");
        if(rs.next())
        {
            double montant = rs.getDouble("MONTANT") + Montant;
            if(BDLink.ExecuteUpdate("UPDATE CONTENUFACTURE SET MONTANT=" + montant +" WHERE RESERVATION ='"+reservation+"' AND NUMFACTURE='"+facture+"'")>0)
                return "Payement effectuee !";
            else
                return "Erreur lors du payement !";  
        }
        else
        {
            if(BDLink.ExecuteUpdate("INSERT INTO CONTENUFACTURE VALUES('"+reservation+"','"+facture+"',"+Montant+")")>0)
            {
                return "Payement effectuee !";
            }
            else
                return "Erreur lors du payement !";
        }
    }
    
   public String getReferantReservation(String idReservation) throws SQLException
   {
       ResultSet rs = BDLink.ExecuteQuery("SELECT REFERANT FROM RESERVATION WHERE IDENTIFIANT = '" + idReservation + "'");
       
       if(rs.next())
           return rs.getNString("REFERANT");
       else
           return null;
   }
    
    public ResultSet getReservationPossible(String Categorie,String Type,String DateDebut, String DateFin) throws SQLException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar debut = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        Date date = new Date();
        try {
            System.out.println("Date today : " + date);
            date=sdf.parse(DateDebut);
            System.out.println("Date de début : " + date);
            System.out.println("Date de début : " + DateDebut);
        } catch (ParseException ex) {
            Logger.getLogger(JDBCRequests.class.getName()).log(Level.SEVERE, null, ex);
        }
        debut.setTime(date);
        sdf.setCalendar(today);
        if(debut.before(today))
        {
            
            return null ;
        }
        String Select = "SELECT ID, PRIX, TAILLE AS \"NOMBRE DE PERSONNES\", EQUIPEMENT";
        String intervalle1 = "to_date('"+DateDebut+" 14:00','DD/MM/YYYY  hh24:MI') BETWEEN DEBUT AND FIN";
        String intervalle2 ="to_date('"+DateFin+" 12:00','DD/MM/YYYY  hh24:MI') BETWEEN DEBUT AND FIN";
        String intervalle3 ="DEBUT BETWEEN  to_date('"+DateDebut+" 14:00','DD/MM/YYYY hh24:MI') AND to_date('"+DateFin+" 12:00','DD/MM/YYYY hh24:MI')";
        String intervalle4 ="FIN BETWEEN  to_date('"+DateDebut+" 14:00','DD/MM/YYYY hh24:MI') AND to_date('"+DateFin+" 12:00','DD/MM/YYYY hh24:MI')";
        
        String requeteImbriquee = "( SELECT CHAMBRE FROM RESERVATION WHERE ( "+intervalle1+" OR "+intervalle2+" OR "+intervalle3+" OR "+intervalle4+"))";
        return BDLink.ExecuteQuery(Select + " FROM CHAMBRES WHERE CATEGORIE = '" + Categorie + "' AND TYPECHAMBRE='" + Type + "' AND ID NOT IN " + requeteImbriquee);

    }
    
    public ResultSet getReservationPossiblesansType(String DateDebut, String DateFin) throws SQLException
    {
        return BDLink.ExecuteQuery("SELECT DISTINCT CATEGORIE,TYPECHAMBRE, ID, PRIX, TAILLE AS \"NOMBRE DE PERSONNE\", EQUIPEMENT FROM CHAMBRES WHERE ID NOT IN  ( SELECT CHAMBRE FROM RESERVATION WHERE to_date('"+DateDebut+"','DD/MM/YYYY') BETWEEN to_date(DEBUT,'DD/MM/YYYY') AND to_date(FIN,'DD/MM/YYYY') OR to_date('"+DateFin+"','DD/MM/YYYY') BETWEEN to_date(DEBUT,'DD/MM/YYYY') AND to_date(FIN,'DD/MM/YYYY')  OR to_date(DEBUT,'DD/MM/YYYY') BETWEEN  to_date('"+DateDebut+"','DD/MM/YYYY') AND to_date('"+DateFin+"','DD/MM/YYYY') OR to_date(FIN,'DD/MM/YYYY') BETWEEN  to_date('"+DateDebut+"','DD/MM/YYYY') AND to_date('"+DateFin+"','DD/MM/YYYY') ) ORDER BY CATEGORIE,TYPECHAMBRE,PRIX");
    }
    
    //return true if reservation with that id is found
    public boolean checkReservationID(String ID) throws SQLException
    {
        ResultSet rs ;
        rs = BDLink.ExecuteQuery("SELECT * FROM RESERVATION WHERE IDENTIFIANT = '" + ID + "'");
        return rs.next();
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Panier">
    public ResultSet GetPanier(String username) throws SQLException
    {
        String client = getUserClientID(username);
        return BDLink.ExecuteQuery("SELECT IDENTIFIANT, REFERANT, CHAMBRE, to_char(DEBUT, 'dd/mm/yyyy') AS \"DATE DE DEBUT\", to_char(FIN, 'dd/mm/yyyy') AS \"DATE DE FIN\", PRIXNET AS PRIX FROM RESERVATION WHERE REFERANT = '"+client+"' AND PAYE='N'");
    }
    
    public double GetPanierPrix(String username) throws SQLException
    {
        ResultSet rs = GetPanier(username);
        double prix = 0 ;
        if(rs.next())
        {
            String reservation ;
            do
            {
                reservation = rs.getString("IDENTIFIANT");
                prix = prix + GetPrixReservation(reservation) ;
                
            }while(rs.next());
        }
        
        return prix ;
    }
    
    public ResultSet payPanier(String username, String credit) throws SQLException
    {
        ResultSet rs = this.GetPanier(username);
        String reservation;
        if(rs.next())
        {
            
            String facture = CreateFacture(username,credit);
            
            do
            {
                reservation = rs.getString("IDENTIFIANT");
                PayReservation(reservation,facture,GetPrixReservation(reservation));
            }while(rs.next());
            return getFacture(facture);
        }
        
        return null;
    }
    
    
    
    public void viderPanier(String username) throws SQLException
    {
        String client = getUserClientID(username);
        BDLink.ExecuteUpdate("DELETE FROM RESERVATION WHERE REFERANT='"+client+"' AND PAYE='N'");
    }
    
    public String CreateFacture(String username, String Credit) throws SQLException
    {
        String client = getUserClientID(username);
        if(client == null) client = username ;
        ResultSet set = BDLink.ExecuteQuery("SELECT count(*) FROM FACTURE");
        set.next();
        String id = Integer.toString((set.getInt(1)+1));
        
        if(BDLink.ExecuteUpdate("INSERT INTO FACTURE VALUES(" + id + ", " + client + ", " + Credit+ ")") >0)
            return id;
        else
            return null;
        
    }
    
    public ResultSet getFacture(String facture) throws SQLException
    {
        String champs = "IDENTIFIANT, REFERANT, CHAMBRE, to_char(DEBUT, 'dd/mm/yyyy') AS \"DATE DE DEBUT\", to_char(FIN, 'dd/mm/yyyy') AS \"DATE DE FIN\", PRIXNET AS PRIX, PAYE";
        return BDLink.ExecuteQuery("SELECT "+ champs +" FROM RESERVATION WHERE IDENTIFIANT IN (SELECT RESERVATION FROM CONTENUFACTURE WHERE NUMFACTURE='"+facture+"')");
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Application_payement">
    public ResultSet getReservationNonPayees() throws SQLException
    {
        String Select = "SELECT IDENTIFIANT,Prenom||' '||Nom AS \"CLIENT\", CHAMBRE, to_char(Debut,'DD/MM/YYYY') AS \"DATE DE DEBUT\", to_char(Fin,'DD/MM/YYYY') AS \"DATE DE FIN\", prixNet as \"TOTAL\", prixNet-NVL(Montant,0)  as \"RESTE\" from contenuFacture full join (SELECT * FROM CLIENTS INNER JOIN RESERVATION ON CLIENTS.ID = RESERVATION.REFERANT) on identifiant = contenuFacture.reservation";
        return BDLink.ExecuteQuery(Select + " WHERE PAYE='N'");
    }
    
    public ResultSet getReservationNonPayeesPar(String prenom, String nom) throws SQLException
    {
        ResultSet rs = getClientID(prenom, nom) ;
        
        if(rs.next())
            return getReservationNonPayeesPar(rs.getNString("ID"));
        else
            return null ;
    }
    
    public ResultSet getReservationNonPayeesPar(String idClient) throws SQLException
    {
        String Select = "SELECT IDENTIFIANT,Prenom||' '||Nom AS \"CLIENT\", CHAMBRE, to_char(Debut,'DD/MM/YYYY') AS \"DATE DE DEBUT\", to_char(Fin,'DD/MM/YYYY') AS \"DATE DE FIN\", prixNet as \"TOTAL\", prixNet-NVL(Montant,0)  as \"RESTE\" from contenuFacture full join (SELECT * FROM CLIENTS INNER JOIN RESERVATION ON CLIENTS.ID = RESERVATION.REFERANT) on identifiant = contenuFacture.reservation";
        return BDLink.ExecuteQuery(Select + " WHERE PAYE='N' AND ID='"+idClient+"'");
    }
    
    public int getNumFacture(String reservation) throws SQLException
    {
        ResultSet rs = BDLink.ExecuteQuery("SELECT NUMFACTURE FROM CONTENUFACTURE WHERE RESERVATION = '"+reservation+"' ");
        
        if(rs.next())
            return rs.getInt("NUMFACTURE");
        else
            return -1 ;
    }
    
    public double getSolde(String reservation) throws SQLException
    {
        ResultSet isFacture = BDLink.ExecuteQuery("SELECT * FROM contenufacture WHERE reservation = '"+reservation+"'");
        double montant = 0 ;
        if(isFacture.next())
            montant = isFacture.getDouble("MONTANT");
        ResultSet rs = BDLink.ExecuteQuery("SELECT prixnet from reservation where identifiant = '"+reservation+"'");
        
        if(rs.next())
            return rs.getDouble("PRIXNET") - montant;
        else
            return -1 ;
    }
    
    public boolean checkAmount(String reservation, double montant) throws SQLException {
        double solde = getSolde(reservation) ;
        
        System.out.println("SOLDE = " + solde + " - " + montant);
        return (solde-montant)>= 0 ;
    }
    // </editor-fold>

    
}