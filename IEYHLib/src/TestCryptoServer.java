
import CryptoLib.CryptoManager;
import CryptoLib.CryptoManagerException;
import CryptoLib.InvalidCredentialsException;
import CryptoLib.SessionCrypto;
import MessageLib.*;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
 *
 * @author Thomas
 */
public class TestCryptoServer {
    public static void main(String[] args) {
        try {
            
            CryptoManager cryptoManagerServ = new CryptoManager("../Evaluation6/keystore/ServPay.p12");
            MySocketServer sockserv;
            sockserv = new MySocketServer(50000,true);
            System.out.println("ServSocket open");
            MySocket socketservice = sockserv.Accept();
            System.out.println("Connection Accepted");
            
            SessionCrypto session = cryptoManagerServ.handshakeServer(socketservice, "thomas");
            System.out.println("Reçu :" + session);
            
            Message message;
            message = (Message) socketservice.ReceiveObj();
            
            System.out.println("Reçu Sym crypt : "+message);
            
            Client client;
            client = (Client) cryptoManagerServ.symDecrypt(session.getCryptKey(), (SealedObject)message.getContent());
            System.out.println("Reçu Client Decrypt : "+client);
            
            //HMAC Test
            message = (Message) socketservice.ReceiveObj();
            
            byte[] hmaccli = (byte[]) message.getContent();
            
            int numTransac = 12;
            
            boolean verifhmac = cryptoManagerServ.verifyHMAC(session.getHmacKey(),session.getUser(),numTransac, hmaccli);
            
            System.out.println("Hmac cli : " + hmaccli.toString());
            System.out.println("Verif : " + verifhmac);
            
            //Signature
            message = (Message) socketservice.ReceiveObj();
            Pay pay = (Pay) cryptoManagerServ.symDecrypt(session.getCryptKey(),(SealedObject) message.getContent());
            System.out.println("Pay received : " + pay);
            System.out.println("Signature : "+message.getSignature());
            
            boolean verifSignature = cryptoManagerServ.verifySignatureEmploye(session.getUser(), pay.toString(), message.getSignature());
            
            System.out.println("Check sign : "+verifSignature);
            
            
            
        } catch (KeyStoreException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CryptoManagerException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidCredentialsException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(TestCryptoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
