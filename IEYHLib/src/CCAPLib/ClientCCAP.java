/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CCAPLib;

import CryptoLib.CryptoManager;
import MessageLib.Debit;
import MessageLib.Message;
import SocketFacilities.MySocket;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steph
 */

public class ClientCCAP {

    private MySocket socketService;
    private CryptoManager cryptoManager;
    
    public ClientCCAP(MySocket socketService)
    {
        
        this.socketService = socketService;
        try {
        cryptoManager = new CryptoManager("../../Evaluation6/keystore/ServPay.p12");
        } catch (Exception e) {
            System.out.println("Exception : " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
    public Message CheckCard(String carte, double montant)
    {
        Message msg = new Message();
        msg.setID(5);
        
        Debit debit = new Debit(montant, carte);
        
        try {
            msg.setContenu(cryptoManager.asymCryptKeystore(cryptoManager.getCCAPKeyName("servcard"), debit)); //on crypte le contenu
            msg.setSignature(cryptoManager.genSignatureEmploye(cryptoManager.getCCAPKeyName("servpay"), debit.toString()));
            return Transmission(msg);
            } catch (Exception e) {
                msg.setID(-1);
                msg.setContenu("Socket car error !");
                return msg;
            }
        
    }
    
    public Message Transmission(Message msg) throws IOException, ClassNotFoundException
    {
        System.out.println("Transmission CCAP : " + msg);
        socketService.SendObj(msg);
        msg = (Message) socketService.ReceiveObj();
        System.out.println("Reception : "+msg);
        return msg;
    }
}
