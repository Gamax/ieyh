/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Formatage;

import static com.sun.javafx.util.Utils.split;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;


public class LectureResultSet {
    
    //MISE EN FORME
    public static String HTMLRequestTable(ResultSet rs) throws SQLException
    {
        StringBuilder html = new StringBuilder();
        if(rs == null)
            return html.toString();
        
        ResultSetMetaData md = rs.getMetaData();
        int ColumnCount = md.getColumnCount();
        int cpt ;
        
        
        html.append("<table style =\"width:100%\"> <tr>");
        for(cpt=1 ; cpt <= ColumnCount ; cpt ++)
        {
            html.append("<th>" +md.getColumnName(cpt)+"</th>");
        }
        html.append("</tr>");
        
        while(rs.next())
        {
            html.append("<tr>");
            for(cpt = 1 ; cpt <= ColumnCount ; cpt ++ )
            {
                html.append("<th>" + rs.getString(cpt) + "</th>");
            }
            html.append("</tr>");
        }
        
        html.append("</table>");
        
        return html.toString();
    }
    
    
    public static DefaultTableModel GUIRequestTable(ResultSet rs) throws SQLException //Formater ResultSet pour un affichage dans un tableau GUI
    {
        
        DefaultTableModel dtm = new DefaultTableModel();
        if(rs == null)
            return dtm;
        ResultSetMetaData md = rs.getMetaData() ;
        int columnCount = md.getColumnCount() ;
        int cpt ;
        Vector<String> row;
        
        for(cpt = 1 ; cpt <= columnCount ; cpt ++)
        {
            dtm.addColumn(md.getColumnName(cpt));
        }
        while(rs.next())
        {
            row = new Vector<>(columnCount);
            for(cpt = 1 ; cpt <= columnCount ; cpt ++ )
            {
                row.add(rs.getString(cpt));
            }
            dtm.addRow(row);
        }
        
        return dtm;
    }
    
    public  static DefaultListModel GUIRequestList(ResultSet rs) throws SQLException //Formater ResultSet pour un affichage dans une liste
    {
        
         DefaultListModel dlm = new DefaultListModel();
         if(rs == null)
            return dlm;
         ResultSetMetaData md = rs.getMetaData() ;
        int columnCount = md.getColumnCount() ;
        int cpt ;
        
        while(rs.next())
        {
            StringBuilder build = new StringBuilder() ;
            for(cpt = 1 ; cpt <= columnCount ; cpt ++ )
            {
                build.append(rs.getString(cpt));
                if(cpt<columnCount)
                    build.append(" ");
            }
            dlm.addElement(build.toString());
        }
         
         return dlm ;
    }
    
    public static String ConsoleRequestTab(ResultSet rs) throws SQLException  //Formater ResultSet pour un affichage console
    {
        if(rs == null)
            return "no result !";
        StringBuilder StrB = new StringBuilder() ; 
        ResultSetMetaData md = rs.getMetaData() ;
        int columnCount = md.getColumnCount() ;
        int cpt ;
        
        StrB.append("| ");
        for(cpt = 1 ; cpt <= columnCount ; cpt ++)
        {
            StrB.append(md.getColumnName(cpt));
            StrB.append(" | ");
        }
        
        while(rs.next())
        {
            StrB.append(System.getProperty("line.separator"));
            StrB.append("| ");
            for(cpt = 1 ; cpt <= columnCount ; cpt ++ )
            {
                StrB.append(rs.getString(cpt));
                StrB.append(" | ");
            }
        }
        
        return StrB.toString() ;
    }
    
    //TRANSMISSION
    public static String formatageRSToString(ResultSet rs) throws SQLException //Formater ResultSet pour transmettre une chaine de caractères éléments séparateurs plus fin de lignes
    {
        if(rs == null)
            return "no result !";
        StringBuilder StrB = new StringBuilder();
        
        ResultSetMetaData md = rs.getMetaData() ;
        int columnCount = md.getColumnCount() ;
        int cpt ;
        
        for(cpt = 1 ; cpt <= columnCount ; cpt ++)
        {
            StrB.append(md.getColumnName(cpt));
            if(cpt<columnCount)
                StrB.append("!");
        }
        
        while(rs.next())
        {
            StrB.append("#");
            for(cpt = 1 ; cpt <= columnCount ; cpt ++ )
            {
                StrB.append(rs.getString(cpt));
                if(cpt<columnCount)
                    StrB.append("!");
            }
            
        }
        
        return StrB.toString();
    }
    
    public static DefaultListModel formatageStringToList(String str)
    {
        System.out.println("LECTURE RS : " + str );
        
        DefaultListModel dlm = new DefaultListModel () ;
        String strTmp = str.replaceAll("!"," : ");
        String[] TabStr = strTmp.split("#");
        int i ;
       
        
        for(i=1 ; i<TabStr.length ; i++)
        {
            dlm.addElement(TabStr[i]);
        }
        
        return dlm ;
    }
    
    public static DefaultTableModel formatageStringToTable(String str)
    {
        DefaultTableModel dtm = new DefaultTableModel () ;
        
        String[] TabStr = str.split("#");
        int i ;
        
        String[] ColumnHead = TabStr[0].split("!");
        for(i=0 ; i<ColumnHead.length ; i++)
        {
           dtm.addColumn(ColumnHead[i]);
        }
        
        String[] row ;
        for(i=1 ; i<TabStr.length ; i++)
        {
           row = TabStr[i].split("!");
           dtm.addRow(row);
        }
        
        
        return dtm ;
    }
    
    public static String formatageStringToConsoleTab(String str)
    {
        String strTab = str.replaceAll("#", System.getProperty("line.separator"));
        String strTab2 = strTab.replaceAll("!"," | ");
        return strTab2 ;
    }
    
    public static String[] formatageStringToCombobox(String str)
    {
        String strTmp = str.replaceAll("!"," : ");
        String strTemp = strTmp.substring(strTmp.indexOf("#")+1);
        String[] strTab = strTemp.split("#");
        return strTab ;
    }
    
    public static String[] formatageStringToArray(String str)
    {
        String strTmp = str.substring(str.indexOf("#")+1);
        String[] strTab = strTmp.split("#");
        return strTab ;
    }
    
    public static String[] LectureLineArray(String str)
    {
        String[] strTab = str.split("!");
        return strTab ;
    }
    
    

}

