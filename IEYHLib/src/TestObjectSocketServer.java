/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

import MessageLib.Message;
import MessageLib.Pay;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thomas
 */
public class TestObjectSocketServer {
    
    public static void main(String[] args) {
        
        MySocketServer sockserv;
        
        try {
            sockserv = new MySocketServer(50000,true);
        } catch (IOException ex) {
            Logger.getLogger(TestObjectSocketServer.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        
        try {
            
            System.out.println("ServSocket open");
            MySocket socketservice = sockserv.Accept();
            System.out.println("Connection Accepted");
            Message temp;
            temp = (Message) socketservice.ReceiveObj();
            
            System.out.println("OBJ RECU : "+temp.toString());
            System.in.read();
            sockserv.Close();
        } catch (IOException ex) {
            Logger.getLogger(TestObjectSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestObjectSocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}
