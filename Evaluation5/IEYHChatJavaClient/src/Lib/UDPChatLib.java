package Lib;


import CryptoLib.DigestBC;
import SocketFacilities.MyUDPSocket;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thomas
 */
public class UDPChatLib {
    
    MyUDPSocket udpSocket;
    DigestBC digestBC;
    
    public UDPChatLib(MyUDPSocket udpSocket) throws IOException, NoSuchAlgorithmException, NoSuchProviderException{
        this.udpSocket = udpSocket;
        digestBC = new DigestBC();
    }
    
    public void AskQuestion(String text) throws IOException
    {
        Random random = new Random();
        String tag = "Q" + random.nextInt(10) + random.nextInt(10) + random.nextInt(10) + random.nextInt(10);
        
        digestBC.update(text);
        String digest = digestBC.digest();
        System.err.println("digest before send : " + digest);
        udpSocket.Send("1:"+tag+"$"+text +"$"+digest);
    }
    
    public void AnswerQuestion(String tag,String text) throws IOException
    {
        udpSocket.Send("2:"+tag+"$"+text);
    }
    
    public void PostEvent(String text) throws IOException
    {
        Random random = new Random();
        String tag = "E" + random.nextInt(10) + random.nextInt(10) + random.nextInt(10) + random.nextInt(10);
        
        udpSocket.Send("3:"+tag+"$"+text);
    }
    
    public String Receive() throws IOException
    {
        String message = udpSocket.Receive();
        
        String table = message.substring(0,1);
        switch(table)
        {
            case "1": return TraitementQuestion(message.substring(2));
            case "2": return TraitementAnswerQuestion(message.substring(2));
            case "3": return TraitementEvent(message.substring(2));
            default: return "Requete inconnue !";
               
        }
    }
    
    private String TraitementQuestion(String message)
    {
        String tag = message.substring(0, message.indexOf("$"));
        String text = message.substring(message.indexOf("$")+1,message.lastIndexOf("$"));
        String digest = message.substring(message.lastIndexOf("$")+1);
        
        digestBC.update(text);
        String d = digestBC.digest();
        
        System.out.println("Digest sent : " + digest);
        System.out.println("Digest gen : " + d);
        
        if(digestBC.isEqual(d, digest))
            return FormatMessage("[Q/OK][" + tag + "]", text);
        else
            return FormatMessage("[Q/NOK][" + tag + "]", text);
        
    }
    
    private String TraitementAnswerQuestion(String message)
    {
        String tag = message.substring(0, message.indexOf("$"));
        String text = message.substring(message.indexOf("$")+1);
        return FormatMessage("[R][" + tag + "]", text);
    }
    
    private String TraitementEvent(String message)
    {
        String tag = message.substring(0, message.indexOf("$"));
        String text = message.substring(message.indexOf("$")+1);
        return FormatMessage(tag, text);
    }
    
    private String FormatMessage(String tag,String text)
    {
        return new SimpleDateFormat("[HH:mm] ").format(new Date()) + tag + " : " + text;
    }
    
}
