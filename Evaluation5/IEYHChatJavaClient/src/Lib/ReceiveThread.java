/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lib;

import GUI.GUIChat;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thomas
 */
public class ReceiveThread extends Thread {
    
    private UDPChatLib udpChatLib;
    private GUIChat guiChat;
    
    public ReceiveThread(UDPChatLib udpChatLib,GUIChat guiChat){
        this.udpChatLib = udpChatLib;
        this.guiChat = guiChat;
    }
    
    public void run()
    {
        boolean running = true;
        
        while(running){
            try {
                guiChat.DisplayMessage(udpChatLib.Receive());
            } catch (IOException ex) {
                running = false;
                Logger.getLogger(ReceiveThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
