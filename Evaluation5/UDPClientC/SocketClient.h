#ifndef PROJECT_SOCKETCLIENT_H
#define PROJECT_SOCKETCLIENT_H

#include "Socket.h"
#include "BaseException.h"


class SocketClient: public Socket{

public:
    SocketClient();

    void Connect(unsigned short Port) throw (BaseException);

};

#endif
