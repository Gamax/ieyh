#ifndef PROJECT_SOCKETCLIENT_H
#define PROJECT_SOCKETCLIENT_H

#include "BaseException.h"


class SocketUDP {

protected:
    int SocketDescriptor;

public:
    SocketUDP(); //creation socket
    int Send(char * message) ;
    int Receive(char * message) ;
    void Close();
    void Connect(unsigned short Port) throw (BaseException);
};

#endif
