#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "login.h"
using namespace std;

#define SEPCHAMP '$'
login::login(void)
{
    setNom("user");
    setId();
    setPswd("P@ssw0rd");

}

login::login(const string log,const string pswd)
{
    setNom(log);
    setId();
    setPswd(pswd);

}

login::login(login& original)
{
    setNom(original.getNom());
    id = original.getId();
    mdp = original.getPswd();

}

login::~login()
{
}

string login::getNom() const {   return nom; }
string login::getPswd() const {   return mdp; }
int login::getId() const { return id ; }

void login::setNom(const string log) throw (BaseException)
{
    if(log.size() < 1) throw BaseException("Le nom doit faire au moins 1 caractere ");
    nom = log;
}

void login::setPswd(const string pswd) throw (BaseException)
{
    if(pswd.size() < 5) throw BaseException("Le mot de passe doit faire au moins 5 caractere ");

    string Passwd ;

    //TODO : digest password !
    //Passwd = digest => pswd

    mdp = Passwd;
}

void login::setId()
{
    srand (time(NULL));
    id = rand() % 1000 ;
}

bool login::operator==(login& log) const
{
    if(getNom()==log.getNom() && getPswd()==log.getPswd())
        return true;
    return false;
}


ostream& operator<<(ostream& s,const login& log)
{
    s << log.getNom() << SEPCHAMP << log.getPswd();
	return s;
}

ostream& login::Flux(ostream& s) const
{
	s<<*(this)<<endl;
	return s;
}

