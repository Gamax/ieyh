#ifndef PROJECT_SOCKET_H
#define PROJECT_SOCKET_H

class Socket {

protected:
    int SocketDescriptor;

public:
    Socket(); //creation socket
    int Send(char * message) ;
    int Receive(char * message) ;
    void Close();


};


#endif
