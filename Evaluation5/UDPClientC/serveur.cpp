#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include "stump.h"
#include "material.h"
#include "login.h"
#include "SocketServer.h"

using namespace std;


void ThreadServeur();
string stumpLogin(char *query);
string stumpHandMat(char *query);
string stumpListCmd(char *query);
string stumpCancelHandMat(char *query);
string stumpAskMat(char *query);

pthread_mutex_t mutexListConnection;
pthread_cond_t condIndiceConnection;
int nbRequete;
SocketServer* listConnectionSocket[NBTHREAD];
int indiceConnection = -1;

//TODO : (liste) Socket d'utilisation

int main()
{
    int i;
    pthread_t pth[NBTHREAD];

    cout<< "init Server" << endl; //todo

    pthread_mutex_init(&mutexListConnection,NULL);
    pthread_cond_init(&condIndiceConnection,NULL);

    SocketServer* listenSocket = new SocketServer();
    SocketServer* connectionSocket; //connectionSocket temporaire

    //bind de la socket sur l'adresse du serveur avec le port
    listenSocket->Bind(PORT); //todo try catch

    cout<< "listen Server" << endl; //todo


    //Creation des threads
    for(i=0;i<NBTHREAD;i++)
    {
       if(pthread_create(&pth[i],NULL,(void*(*)(void*))ThreadServeur,NULL)!=0)
       {
           cout << "erreur lors de la creation du pool de thread !"<< endl;
           return -1;
       }
    }

    cout<< "thread créé Server" << endl; //todo

    //TODO : corps du serveur principal
    while(1)
    {
        cout<< "corps Server" << endl; //todo
        // Socket d'ecoute
        listenSocket->Listen(); //todo gestion erreur

        connectionSocket = listenSocket->Accept(); //todo gestion erreur

        for(i=0;i<NBTHREAD && listConnectionSocket[i] != NULL;i++); //recherche d'un thread libre
        if(i == NBTHREAD){
            //todo refus de la connection
        }else{
            pthread_mutex_lock(&mutexListConnection);
            listConnectionSocket[i] = connectionSocket; //on stocke notre socket ds le premier emplacement libre
            indiceConnection = i; //l'indice du socket insere
            cout << "Valeur de l'indice" + to_string(i) << endl;
            pthread_mutex_unlock(&mutexListConnection);
            pthread_cond_signal(&condIndiceConnection);
        }

    }

    //Cancel des threads
    for(int i=0;i<NBTHREAD;i++)
    {
       pthread_cancel(pth[i]);
    }


    return 0;
}

void ThreadServeur()
{
    int indice;
    bool endClientConnection;
    SocketServer* connectionSocket;
    char message[MAXSTRING];
    char * typeRequete;
    char * tmp;
    string reply;

    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);
    pthread_setcanceltype(0,NULL);

    //TODO : detacher socket d'ecoute

    while(1)
    {
        pthread_mutex_lock(&mutexListConnection);
        while(indiceConnection == -1)
            pthread_cond_wait(&condIndiceConnection,&mutexListConnection);
        indice = indiceConnection;
        indiceConnection=-1; //on libère l'indice
        connectionSocket = listConnectionSocket[indice];
        pthread_mutex_unlock(&mutexListConnection);
        cout << "init du thread sur un client faite" << endl;

        pthread_testcancel();

        endClientConnection = false;
        while(!endClientConnection){
            //Traitement requete client avec gestion de sa requete et envoi réponse
            cout << "traitement requete client" << endl;

            if(connectionSocket == NULL){
                cout << "connectionSocket est null !!!???" << endl;
                exit(0);
            }

            if(connectionSocket->Receive(message) == -1) //requete reçue
            {
                endClientConnection = true;
            }

            string temp(message);

            cout << "Received message : " + temp << endl;

            //on découpe les paramètres
	        typeRequete = strtok(message, "%") ;
            switch(typeRequete[0]) //todo modify
            {
                case '1' :  reply = stumpLogin(message);
                    break;
                case '2' :  reply = stumpHandMat(message);
                    break;
                case '3' :  reply =  stumpListCmd(message);
                    break;
                case '4' :  reply = stumpCancelHandMat(message);
                    break;
                case '5' :  reply = stumpAskMat(message);
                    break;
            }
            
	    //endClientConnection = true; // ? fait sortir de la boucle ? Recoit qu'une requete..

            tmp = new char[reply.size()+1];
            strcpy(tmp,reply.c_str());

            connectionSocket->Send(tmp);

        }

        //fin de la co au client
        pthread_mutex_lock(&mutexListConnection);
        listConnectionSocket[indice] = NULL;
        pthread_mutex_unlock(&mutexListConnection);

        connectionSocket->Close(); //fermer socket reliée au client

        pthread_testcancel();

        //recherche d'un nouveau client

    }
}




string stumpLogin(char *query)
{
	string user,pwd,reply;
    user = strtok(query, "$") ;
    pwd = strtok(query, "$") ;

    reply = "User " + user + "logged in with pwd : " + pwd;

    cout << reply << endl;

    return reply ;

}
string stumpHandMat(char *query)
{
    string user,pwd,reply;
    user = strtok(query, SEPARATOR) ;
    pwd = strtok(query, SEPARATOR) ;

    reply = "Handmat " + user + "logged in with pwd : " + pwd;

    cout << reply << endl;

    return reply ;
}
string stumpListCmd(char *query)
{
	string tmp ;
    	//TODO : 
	//Lire fichier
	//while (lecture)
	//	tmp = tmp + SEPCHAMP + lecture ;
	//return tmp ;
    return "ListCmd reply";
}
string stumpCancelHandMat(char *query)
{
    	//TODO : 
	//Recuperer id Action, nom Materiel et date

	//Rechercher idAction
	//SI trouve, nom == ? 
	//	dateStockee + delai > date ? 
	//		OUI OK
	//		NON NOK + SEPARATOR + date depassee
    return "CancelHandMat reply";
}
string stumpAskMat(char *query)
{
	//TODO : 
	//Creation objet material
	// creation reussie ? Serialisation
	// OK + Id
    return "AskMat reply";
}
