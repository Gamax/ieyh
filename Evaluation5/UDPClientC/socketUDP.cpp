#include "Socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <cstring>
#include "SocketUDP.h"

#define MAXSTRING 1000


SocketUDP::SocketUDP() {
    SocketDescriptor = socket(AF_INET,SOCK_DGRAM,0);
}

int SocketUDP::Send(char type, char *message) {
    if(type == "Q")
        return send(SocketDescriptor,[]message,MAXSTRING,0);
}

int SocketUDP::Receive(char *message) {
    return recv(SocketDescriptor,message,MAXSTRING,0);
}

void SocketUDP::Close() {
    close(SocketDescriptor);
}

void SocketUDP::Connect(unsigned short Port) throw (BaseException)
{


    struct hostent * infosHost;
    struct sockaddr_in socketAdress;

    char hostname[1024];
    gethostname(hostname, 1023);
    hostname[1023] = '\0';
    if((infosHost = gethostbyname(hostname))==0)
    {
        throw BaseException("Error Bind : gethostbyname failed " + to_string(errno));
    }

    //preparation sock_addr_in
    memset(&socketAdress,0, sizeof(struct sockaddr_in));
    socketAdress.sin_family = AF_INET;
    socketAdress.sin_port = htons(Port); //htons converti le nombre en format r�seau
    memcpy(&socketAdress.sin_addr,infosHost->h_addr,infosHost->h_length);

    if(connect(SocketDescriptor,(struct sockaddr *)&socketAdress, sizeof(struct sockaddr_in))==-1){
        throw BaseException("Error Connect : connect() failed " + to_string(errno));
    }
}
