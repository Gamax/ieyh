#ifndef LOGIN_H
#define LOGIN_H
#include <iostream>
#include <string.h>
#include "BaseException.h"
using namespace std;

class login {

    private :
    string nom ;
    string mdp ;
    int id ;

    public :

    login(void);
    login(const string log, const string pswd);
    login(login& original);
    ~login();

    string getNom() const;
    string getPswd() const;
    int getId() const ;

    void setNom(const string log) throw (BaseException);
    void setPswd(const string pswd) throw (BaseException);
    void setId();

    bool operator==(login& log) const;
    friend ostream& operator<<(ostream& s,const login& mat);
    ostream& Flux(ostream& s) const;
};

#endif // LOGIN_H
