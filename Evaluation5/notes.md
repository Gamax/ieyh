# IEYH - Eval 5

## Syntaxe requête

### TCP

````
1:reservNumber
2:nomEmployeActivites$PasswordDigest
3:nomEmployeChambres$PasswordDigest
4:nomEmployeMateriel$Password
````

### UDP

````
1:tagQuestion$question
2:tagQuestion$reponse
3:tag$text

tagQuestion = "QXXXX"
tag = "EXXXX"
````

## Import Bouncy Castle

#### Copier bcmail-jdk15on-160.jar et bcprov-jdk15on-160.jar dans C:\Program Files\Java\jdk1.8.0_181\jre\lib\ext

#### Modifier C:\Program Files\Java\jdk1.8.0_181\jre\lib\security\java.security

````
security.provider.8=org.jcp.xml.dsig.internal.dom.XMLDSigRI
security.provider.9=sun.security.smartcardio.SunPCSC
security.provider.10=sun.security.mscapi.SunMSCAPI
security.provider.11=org.bouncycastle.jce.provider.BouncyCastleProvider //ceci !!!
#
# Class to instantiate as the system scope:
#
````

#### Déblocages limitations cryptographiques

Ajout des 2 jars contenu dans l'archive jce_policy-8.zip dans le dossier C:\Program Files\Java\jdk1.8.0_181\jre\lib\security