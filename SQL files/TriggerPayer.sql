create or replace TRIGGER reservationPayee
    AFTER INSERT OR UPDATE OF montant ON ContenuFacture
    FOR EACH ROW 
DECLARE 
    prixTotal NUMBER(10,3);
    negativeAmount EXCEPTION ;
    higherAmount EXCEPTION ;
BEGIN
    SELECT PRIXNET INTO prixTotal FROM RESERVATION WHERE IDENTIFIANT = :NEW.RESERVATION ; 
    if(:new.montant < 0 ) THEN RAISE negativeAmount ;
    end if ;
    if(:new.montant > prixTotal ) THEN RAISE higherAmount ;
    end if ;
    if(:new.montant = prixTotal)
    THEN UPDATE RESERVATION SET PAYE='Y' WHERE IDENTIFIANT = :NEW.Reservation ;
    end if ;
EXCEPTION
    WHEN negativeAmount THEN RAISE_APPLICATION_ERROR (-20201, 'amount specified cannot be negative !');
    WHEN higherAmount THEN RAISE_APPLICATION_ERROR (-20202, 'amount specified higher than amount to pay !');
    WHEN NO_DATA_FOUND THEN RAISE ;
END ;
/
