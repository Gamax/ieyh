CREATE TABLE Client
(
    ID int(6) primary key,
    Nom varchar(60) not NULL,
    Prenom varchar(60) not NULL,
    Adresse varchar(200),
    Nationalite varchar(60),
    Naissance Date,
    Mail varchar(60),
    Referant int(6),
    constraint FK$Referant$Client foreign key(Referant) references Client(ID)
);

CREATE TABLE Chambre
(
    ID int(3) primary key,
    Equipement varchar(200),
    Taille int(2),
    Prix int(6) not NULL
);

CREATE TABLE Activite
(
    ID int(3) primary key,
    Nom varchar(60),
    Categorie varchar(60),
    Places int(3),
    Prix int(6) not NULL
);

CREATE TABLE Reservation
(
    Identifiant varchar(60) primary key,
    Referant int(6) not null,
    Chambre int(3),
    Activite int(3),
    Debut Date not null,
    Fin Date not null,
    PrixNet int(5) not null,
    Paye char(1) not null,
    constraint CK$Paye$Reservation check (Paye in ('Y','N')),
    constraint FK$Referant$Reservation foreign key(Referant) references Client(ID),
    constraint FK$Chambre$Reservation foreign key(Chambre) references Chambre(ID),
    constraint FK$Activite$Reservation foreign key(Activite) references Activite(ID)
);
