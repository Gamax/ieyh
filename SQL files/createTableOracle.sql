CREATE TABLE Clients
(
    ID number(6) constraint PK$Clients primary key,
    Nom varchar2(60 char) constraint NN$Nom$Clients not NULL,
    Prenom varchar2(60 char) constraint NN$Prenom$Clients not NULL,
    Adresse varchar2(200 char),
    Nationalite varchar2(60 char),
    Naissance Date,
    Mail varchar2(60 char),
    Referant number(6),
    constraint FK$Referant$Clients foreign key(Referant) references Clients(ID)
);

create table UserVillages
(
    login   varchar2(30 char) constraint PK$UserVillages primary key,
    password varchar2(30 char) constraint NN$password$UserVillages not null
);


create table UserAgences
(
    login   varchar2(30 char) constraint PK$UserAgences primary key,
    password varchar2(30 char) constraint NN$password$UserAgences not null
);

create table UserMateriel
(
    login   varchar2(30 char) constraint PK$UserMateriel primary key,
    password varchar2(30 char) constraint NN$password$UserMateriel not null
);

CREATE TABLE Chambres
(
    ID number(3) constraint PK$Chambres primary key,
	Categorie varchar2(20 char) constraint CK$Categorie CHECK (Categorie IN ('VILLAGE','MOTEL')),
	TypeChambre varchar2(20 char) constraint CK$TypeChambre CHECK (TypeChambre IN('SIMPLE','DOUBLE','FAMILIALE')),
    Equipement varchar2(200 char),
    Taille number(2),
    Prix number(6,2) constraint NN$Prix$Chambres not NULL   
);

CREATE TABLE Activites
(
    ID number(3) constraint PK$Activites primary key,
    Nom varchar2(60 char),
    Categorie varchar2(60 char),
    Places number(3),
    Prix number(6) constraint NN$Prix$Activites not NULL,
	DateDebut Date constraint NN$Debut$Activites not NULL,
	DateFin Date constraint NN$Fin$Activites not NULL,
	CONSTRAINT CK$DureeActivites CHECK (DateFin >= DateDebut) 
);

CREATE TABLE ActiviteReservee
(
	Activite number(3) constraint FK$Activite$ActiviteReservee REFERENCES Activites(id),
	client varchar2 (60 CHAR) CONSTRAINT NN$Client$ActiviteReservee NOT NULL,
	CONSTRAINT PK$ActiviteReservee PRIMARY KEY (Activite, client)
);

CREATE TABLE Reservation
(
    Identifiant varchar2(60 char) constraint PK$Reservations primary key,
    Referant number(6) constraint NN$Referant$Reservations not null,
    Chambre number(3),
    Debut Date constraint NN$Debut$Reservations not null,
    Fin Date constraint NN$Fin$Reservations not null,
    PrixNet number(6,2) constraint NN$PrixNet$Reservations not null,
    Paye char(1) constraint NN$Paye$Reservations not null,
    constraint CK$Paye$Reservations check (Paye in ('Y','N')),
    constraint FK$Referant$Reservations foreign key(Referant) references Clients(ID),
    constraint FK$Chambre$Reservations foreign key(Chambre) references Chambres(ID),
	CONSTRAINT CK$DureeReservation CHECK (Fin >= Debut) 
);

CREATE TABLE Facture
(
	NumeroFacture number(6) Constraint PK$Facture PRIMARY KEY, 
	Referant number(6) constraint NN$Referant$Facture NOT NULL,
	CreditCard number(16) constraint NN$CreditCard$Factures not null,
	constraint FK$Referant$Factures FOREIGN KEY(Referant) REFERENCES Clients(ID)
);

CREATE TABLE ContenuFacture
(
	Reservation varchar2(60) constraint FK$Reservation$ContenuFacture REFERENCES Reservation(Identifiant),
	NumFacture number(6) constraint FK$NumFacture$ContenuFacture REFERENCES Facture(NumeroFacture),
	Montant number(10,3),
	CONSTRAINT PK$ContenuFacture PRIMARY KEY(Reservation,NumFacture)
);
CREATE TABLE UserClient
(
	Username varchar2(60) constraint PK$LoginUser PRIMARY KEY,
	Password varchar2(60) constraint NN$Password$LoginUser NOT NULL, 
	Client	number(6) constraint NN$Client$LoginUser NOT NULL,
	constraint FK$Client$UserClient FOREIGN KEY(Client) REFERENCES Clients(Id)	
);

CREATE TABLE USERADMIN (
    login VARCHAR2(20) CONSTRAINT PK$USERADMIN PRIMARY KEY,
    password VARCHAR2(20) CONSTRAINT NN$PASSWD$USERADMIN NOT NULL 
);
commit;