/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBfacility;
import java.sql.*;

/**
 *
 * @author Thomas
 */
public class JDBCLibrary {
    
    private Connection connection;
    
    public static void LoadOracleDriver() throws ClassNotFoundException
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
    }
    
    public static void LoadMySQLDriver() throws ClassNotFoundException
    {
        Class.forName("com.mysql.jdbc.Driver");
    }
    
    public void ConnectMySQL(String address, String port,String database,String user,String password) throws SQLException
    {
        connection = DriverManager.getConnection("jdbc:mysql://"+address+":"+port+"/"+database+"?serverTimezone=UTC",user,password); //todo not sure
    }
    
    public void ConnectOracle(String address, String port,String service,String user,String password) throws SQLException
    {
        
            connection = DriverManager.getConnection("jdbc:oracle:thin:@//"+address+":"+port+"/"+service,user,password); 
        
    }
    
    synchronized public ResultSet ExecuteQuery(String query) throws SQLException
    {
        Statement instruction = connection.createStatement(); 
        return instruction.executeQuery(query);

    }
    
    synchronized public int ExecuteUpdate(String query) throws SQLException
    {
        Statement instruction = connection.createStatement(); 
        return instruction.executeUpdate(query);
    }
    
    
}
