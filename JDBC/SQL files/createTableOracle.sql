CREATE TABLE Clients
(
    ID number(6) constraint PK$Clients primary key,
    Nom varchar2(60 char) constraint NN$Nom$Clients not NULL,
    Prenom varchar2(60 char) constraint NN$Prenom$Clients not NULL,
    Adresse varchar2(200 char),
    Nationalite varchar2(60 char),
    Naissance Date,
    Mail varchar2(60 char),
    Referant number(6),
    constraint FK$Referant$Clients foreign key(Referant) references Clients(ID)
);

create table UserVillages
(
    login   varchar2(30 char) constraint PK$UserVillages primary key,
    pasword varchar2(30 char) constraint NN$password$UserVillages not null
);


create table UserAgences
(
    login   varchar2(30 char) constraint PK$UserAgences primary key,
    pasword varchar2(30 char) constraint NN$password$UserAgences not null
);

CREATE TABLE Chambres
(
    ID number(3) constraint PK$Chambres primary key,
    Equipement varchar2(200 char),
    Taille number(2),
    Prix number(6) constraint NN$Prix$Chambres not NULL
);

CREATE TABLE Activites
(
    ID number(3) constraint PK$Activites primary key,
    Nom varchar2(60 char),
    Categorie varchar2(60 char),
    Places number(3),
    Prix number(6) constraint NN$Prix$Activites not NULL
);

CREATE TABLE Reservation
(
    Identifiant varchar2(60 char) constraint PK$Reservations primary key,
    Referant number(6) constraint NN$Referant$Reservations not null,
    Chambre number(3),
    Activite number(3),
    Debut Date constraint NN$Debut$Reservations not null,
    Fin Date constraint NN$Fin$Reservations not null,
    PrixNet number(5) constraint NN$PrixNet$Reservations not null,
    Paye char(1) constraint NN$Paye$Reservations not null,
    constraint CK$Paye$Reservations check (Paye in ('Y','N')),
    constraint FK$Referant$Reservations foreign key(Referant) references Clients(ID),
    constraint FK$Chambre$Reservations foreign key(Chambre) references Chambres(ID),
    constraint FK$Activite$Reservations foreign key(Activite) references Activites(ID)
);

commit;