insert into Client values(1,'Dethier','Thomas','INPRES Lpo1','Belge',null,'thomas@hepl.be',1);
insert into Client values(2,'Bemelmans','Stéphanie','INPRES Lpo2','Belge',null,null,1);
insert into Client values(3,'Schyns','Noé','INPRES Lpo3','Belge',null,null,1);
insert into Client values(4,'Gilson','Loic','INPRES AN','Belge',null,null,4);
insert into Client values(5,'Dussaussois','Florian','INPRES AE','Belge',null,null,4);

insert into Chambre values (11,'Cuisine%Douche',2,250);
insert into Chambre values (12,'Douche',2,150);
insert into Chambre values (13,'Frigo',1,50);
insert into Chambre values (21,'Douche%Frigo%Cusine',4,500);

insert into Activite values (1,'VTT','Sport',30,40);
insert into Activite values (2,'Kayak','Sport',20,30);
insert into Activite values (3,'Randonnée','Sport',50,15);
insert into Activite values (4,'Musée de la chèvre','Détente',100,12);

insert into Reservation values ('20181012-RES1',1,11,null,str_to_date('11/10/1995','%d/%m/%Y'),str_to_date('13/10/1995','%d/%m/%Y'),150,'Y');
insert into Reservation values ('20181012-RES2',1,12,1,str_to_date('11/10/1995','%d/%m/%Y'),str_to_date('13/10/1995','%d/%m/%Y'),160,'N');
insert into Reservation values ('20181012-RES3',4,null,2,str_to_date('11/10/1995','%d/%m/%Y'),str_to_date('13/10/1995','%d/%m/%Y'),300,'Y');
insert into Reservation values ('20181012-RES4',4,null,1,str_to_date('11/10/1995','%d/%m/%Y'),str_to_date('13/10/1995','%d/%m/%Y'),150,'Y');
commit;