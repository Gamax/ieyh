
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainApps;

import DBFacilities.RequestServer;
import GUIFacilities.ServerGUI;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Server {

    private RequestServer requestManager;

    public void runServer(int nbThread, int port, String serverName)
    {

        ServerGUI servergui = new ServerGUI(serverName,port);
        servergui.setVisible(true);

        boolean coDB = false;

        while(!coDB) {
            try {
                requestManager = new RequestServer();
                coDB = true;
            } catch (SQLException | ClassNotFoundException e) {
                servergui.printLog("Connection to database failed retrying in 5 seconds...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

        servergui.printLog("Connection to database initialized");


        MySocketServer listeningSocket;
        try {
            listeningSocket = new MySocketServer(port);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        servergui.printLog("Socket server initialized");

        ExecutorService threadPool = Executors.newFixedThreadPool(nbThread);

        servergui.printLog("Thread pool initialized");

        while(true){
            MySocket clientSocket;
            try {
                clientSocket = listeningSocket.Accept();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            threadPool.execute(new ServerThread(clientSocket,requestManager,servergui));
            servergui.printLog("Connection accepted");
        }

    }
}
