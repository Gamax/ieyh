import MainApps.Server;
import GUIFacilities.ServerSetupGUI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class testserver {

public static void main(String args[]) {

    ServerSetupGUI setupgui = new ServerSetupGUI();
    setupgui.setVisible(true);
    
    while(setupgui.isVisible()){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(testserver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Server server = new Server();
    server.runServer(setupgui.nbthread,setupgui.port,"ServReserv");
}

}
